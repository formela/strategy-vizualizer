import { Location, Node } from "./node_location.js";

import chroma from "chroma-js";
import iwanthue from "iwanthue";
import { calculateBSCCs } from "./bscc.js";
import { stationaryDistributionFromNodes } from "./markov_chain_utils.js";

export { loadJsonExperiment };

function loadJsonExperiment(p5, data) {
  if (data[0]) {
    console.log("Loading V2 experiment");
    return _loadJsonExperimentV2_2(p5, data);
  } else {
    console.log("Loading V1 experiment");
    return _loadJsonExperimentV1(p5, data);
  }
}

const namedColors = {
  gray: "#718096",
  red: "#E53E3E",
  orange: "#DD6B20",
  yellow: "#D69E2E",
  green: "#38A169",
  teal: "#319795",
  blue: "#3182ce",
  cyan: "#00B5D8",
  purple: "#805AD5",
  pink: "#D53F8C",
}

function _loadJsonExperimentV1(p5, raw_data) {
  // Find all targets of edges.
  // If there are nodes that are not included in the targets, do not include them.
  // If there are no edges coming to the node, do not include it.

  let data = raw_data.graph;
  console.log(raw_data);

  let nodes = [];
  let locations = [];

  const validNodes = new Set();
  for (const edgeData of data.edges) {
    validNodes.add(edgeData.tgt);
  }

  // Load nodes
  for (const locationData of data.nodes) {
    const locationId = locationData.id;
    const locationName = locationData.name;
    const locationColor = locationData.color;

    if (!locationData.hasOwnProperty("nodes")) {
      continue;
    }
    const position = p5.createVector(
      p5.random(p5.canvasWidth) - p5.canvasWidth / 2,
      p5.random(p5.canvasHeight) - p5.canvasHeight / 2
    );
    let lockedPosition = null;
    if (locationData.location && raw_data.background) {
      const minY = raw_data.background.top_left[0];
      const minX = raw_data.background.top_left[1];
      const maxY = raw_data.background.bottom_right[0];
      const maxX = raw_data.background.bottom_right[1];

      // Interpolate the position based on the background image
      const x = (locationData.location[1] - minX) / (maxX - minX);
      const y = (locationData.location[0] - minY) / (maxY - minY);

      // The node has a specific position
      lockedPosition = p5.createVector(x, y);
    }

    let color = null;
    if (locationColor) {
      if (namedColors[locationColor]) {
        color = p5.color(chroma(namedColors[locationColor]).brighten().hex());
      } else {
        color = p5.color(locationColor);
      }
    }

    const location = new Location(p5, position, color, locationId, locationName, lockedPosition);
    for (const nodeData of locationData.nodes) {
      if (!validNodes.has(nodeData.id)) continue;

      const nodeId = nodeData.id;
      const node = new Node(p5, position, color, nodeId, locationName);
      location.addNode(node);
      nodes.push(node);
    }
    locations.push(location);
  }

  // Recolor locations and nodes now that we know how many there are
  // TODO: When migrating to the new data format, we'll know the number of locations beforehands

  const colors = _generateColors(locations.length);
  locations.forEach((location, i) => {
    let locationColor = location.color;
    if (!locationColor) {
      locationColor = p5.color(colors[i]);
    }
    location.color = locationColor;
    location.nodes.forEach((node) => {
      node.color = p5.color(chroma(locationColor.toString('#rrggbb')).darken().hex());
    });
  });

  // Load edges with weights
  for (const edgeData of data.edges) {
    if (!(validNodes.has(edgeData.tgt) && validNodes.has(edgeData.src))) {
      continue;
    }
    const startNode = nodes.filter((node) => node.name === edgeData.src)[0];
    const endNode = nodes.filter((node) => node.name === edgeData.tgt)[0];
    const weight = edgeData.probability;
    if (!startNode || !endNode) continue;
    startNode.addNeighbor(endNode, weight);
  }

  nodes.forEach((node) => {
    node.normalizeEdgeWeights();
    node.findNeighborLocations();
  });

  // Find stationary distribution
  const stationaryDistributionNodes = stationaryDistributionFromNodes(nodes);

  nodes.forEach((node, i) => {
    node.stationaryDistributionWeight = stationaryDistributionNodes[i];
  });

  locations.forEach((location) => {
    location.findNeighbors();
    location.recomputeStationaryConnections();
  });

  nodes.forEach((node) => {
    node.recomputeStationaryConnections();
  });

  let maxSdEdgeWeight = 0;
  let maxSdLocationWeight = 0;
  ({ maxSdEdgeWeight, maxSdLocationWeight } = _findMaximumSDWeights(locations));

  calculateBSCCs(locations);

  // Shrink or expand the nodes and locations based on their accessibility.
  locations.forEach((location) => {
    if (location.isBSCC) {
      if (location.isUnfolded) {
        location.radius =
          window.radiusObject.locationOpenRadius *
          Math.sqrt(location.nodes.length);
      } else {
        location.radius = window.radiusObject.locationBaseRadius;
      }
    } else {
      if (location.isUnfolded) {
        location.radius =
          window.radiusObject.locationSmallOpenRadius *
          Math.sqrt(location.nodes.length);
      } else {
        location.radius = window.radiusObject.locationSmallBaseRadius;
      }
    }
  });
  nodes.forEach((node) => {
    if (node.isBSCC) {
      if (node.parentLocation.isUnfolded) {
        node.radius = window.radiusObject.nodeOpenRadius;
      } else {
        node.radius = window.radiusObject.nodeBaseRadius;
      }
    } else {
      if (node.parentLocation.isUnfolded) {
        node.radius = window.radiusObject.nodeSmallOpenRadius;
      } else {
        node.radius = window.radiusObject.nodeSmallBaseRadius;
      }
    }
  });

  const background = raw_data.background ? p5.loadImage(raw_data.background.file) : null;
  const name = raw_data.name;

  return { nodes, locations, maxSdEdgeWeight, maxSdLocationWeight, background, name };
}

const Edge_Probability = 1;
const Edge_Identity = 0;
const Location_Name = 0;

function _loadJsonExperimentV2_2(p5, data) {
  data = data[0];
  let nodes = [];
  let locations = [];
  const validNodes = new Set();
  const location_node_mapping = new Map();

  for (let [[src, tgt], probability] of data.strategy) {
    const tgtLocation = tgt[Location_Name];
    const srcLocation = src[Location_Name];

    // ak chces aby sa neriesili nodes, do ktorych nic nejde, zakomentuj srclcoation
    if (!location_node_mapping.get(tgtLocation)) {
      location_node_mapping.set(tgtLocation, new Set());
    }
    if (!location_node_mapping.get(srcLocation)) {
      location_node_mapping.set(srcLocation, new Set());
    }

    const location = location_node_mapping.get(tgtLocation);
    location.add(`${tgt}`);

    validNodes.add(tgt);

    const location_ = location_node_mapping.get(srcLocation);
    location_.add(`${src}`);
  }

  console.log(validNodes);

  for (let locationData of data.graph.nodes) {
    const locationName = locationData[Location_Name];
    const nodes__ = Array.from(
      location_node_mapping.get(locationName) ?? new Set()
    );

    const position = p5.createVector(
      p5.random(p5.canvasWidth) - p5.canvasWidth / 2,
      p5.random(p5.canvasHeight) - p5.canvasHeight / 2
    );
    const location = new Location(p5, position, p5.color("#bbb"), locationName);
    locations.push(location);

    nodes__.forEach((nodeName) => {
      const position = p5.createVector(
        p5.random(p5.canvasWidth) - p5.canvasWidth / 2,
        p5.random(p5.canvasHeight) - p5.canvasHeight / 2
      );
      const node = new Node(p5, position, p5.color("#bbb"), nodeName);
      nodes.push(node);
      location.addNode(node);
    });
  }

  const colors = _generateColors(locations.length);
  locations.forEach((location, i) => {
    location.color = p5.color(colors[i]);
    location.nodes.forEach((node) => {
      node.color = p5.color(chroma(colors[i]).darken().hex());
    });
  });

  for (let [[src, tgt], probability] of data.strategy) {
    const startNode = nodes.filter((node) => node.name === `${src}`)[0];

    const endNode = nodes.filter((node) => node.name === `${tgt}`)[0];
    const weight = probability;
    if (!startNode || !endNode) continue;
    startNode.addNeighbor(endNode, weight);
  }

  nodes.forEach((node) => {
    node.normalizeEdgeWeights();
    node.findNeighborLocations();
  });

  // Find stationary distribution
  const stationaryDistributionNodes = stationaryDistributionFromNodes(nodes);

  nodes.forEach((node, i) => {
    node.stationaryDistributionWeight = stationaryDistributionNodes[i];
  });

  locations.forEach((location) => {
    location.findNeighbors();
    location.recomputeStationaryConnections();
  });

  nodes.forEach((node) => {
    node.recomputeStationaryConnections();
  });

  let maxSdEdgeWeight = 0;
  let maxSdLocationWeight = 0;
  ({ maxSdEdgeWeight, maxSdLocationWeight } = _findMaximumSDWeights(locations));

  calculateBSCCs(locations);

  // Shrink or expand the nodes and locations based on their accessibility.
  locations.forEach((location) => {
    if (location.isBSCC) {
      if (location.isUnfolded) {
        location.radius =
          window.radiusObject.locationOpenRadius *
          Math.sqrt(location.nodes.length);
      } else {
        location.radius = window.radiusObject.locationBaseRadius;
      }
    } else {
      if (location.isUnfolded) {
        location.radius =
          window.radiusObject.locationSmallOpenRadius *
          Math.sqrt(location.nodes.length);
      } else {
        location.radius = window.radiusObject.locationSmallBaseRadius;
      }
    }
  });

  nodes.forEach((node) => {
    if (node.isBSCC) {
      if (node.parentLocation.isUnfolded) {
        node.radius = window.radiusObject.nodeOpenRadius;
      } else {
        node.radius = window.radiusObject.nodeBaseRadius;
      }
    } else {
      if (node.parentLocation.isUnfolded) {
        node.radius = window.radiusObject.nodeSmallOpenRadius;
      } else {
        node.radius = window.radiusObject.nodeSmallBaseRadius;
      }
    }
  });

  return { nodes, locations, maxSdEdgeWeight, maxSdLocationWeight };
}

function _loadJsonExperimentV2(p5, data) {
  // Find all targets of edges.
  // If there are nodes that are not included in the targets, do not include them.
  // If there are no edges coming to the node, do not include it.

  let nodes = [];
  let locations = [];

  // Get list of nodes that have at least one edge coming to them
  const validNodes = new Set();
  for (let edgeData of data[0].strategy) {
    validNodes.add(edgeData[0][1][0]);
  }

  for (let nodeData of data[0].graph.nodes) {
    const nodeName = nodeData[0];
    let node = null;

    // Create the node
    if (!validNodes.has(nodeName)) continue;
    const position = p5.createVector(
      p5.random(p5.canvasWidth) - p5.canvasWidth / 2,
      p5.random(p5.canvasHeight) - p5.canvasHeight / 2
    );
    node = new Node(p5, position, p5.color("#bbb"), nodeName);
    nodes.push(node);

    // Find the location
    const locationName = nodeName.split("_")[0];
    let location = locations.find((location) => location.name === locationName);
    // If there is no location with the same name, create a new location
    if (!location) {
      const position = p5.createVector(
        p5.random(p5.canvasWidth) - p5.canvasWidth / 2,
        p5.random(p5.canvasHeight) - p5.canvasHeight / 2
      );
      location = new Location(p5, position, p5.color("#bbb"), locationName);
      locations.push(location);
    }
    location.addNode(node);
  }

  // Recolor locations and nodes now that we know how many there are
  const colors = _generateColors(locations.length);
  locations.forEach((location, i) => {
    location.color = p5.color(colors[i]);
    location.nodes.forEach((node) => {
      node.color = p5.color(chroma(colors[i]).darken().hex());
    });
  });

  // Load edges with weights
  for (let edgeData of data[0].strategy) {
    const startNode = nodes.filter(
      (node) => node.name === edgeData[0][0][0]
    )[0];
    const endNode = nodes.filter((node) => node.name === edgeData[0][1][0])[0];
    const weight = edgeData[1];
    if (!startNode || !endNode) continue;
    startNode.addNeighbor(endNode, weight);
  }

  nodes.forEach((node) => {
    node.normalizeEdgeWeights();
    node.findNeighborLocations();
  });

  // Find stationary distribution
  const stationaryDistributionNodes = stationaryDistributionFromNodes(nodes);

  nodes.forEach((node, i) => {
    node.stationaryDistributionWeight = stationaryDistributionNodes[i];
  });

  locations.forEach((location) => {
    location.findNeighbors();
    location.recomputeStationaryConnections();
  });

  nodes.forEach((node) => {
    node.recomputeStationaryConnections();
  });

  let maxSdEdgeWeight = 0;
  let maxSdLocationWeight = 0;
  ({ maxSdEdgeWeight, maxSdLocationWeight } = _findMaximumSDWeights(locations));

  calculateBSCCs(locations);

  // Shrink or expand the nodes and locations based on their accessibility.
  locations.forEach((location) => {
    if (location.isBSCC) {
      if (location.isUnfolded) {
        location.radius =
          window.radiusObject.locationOpenRadius *
          Math.sqrt(location.nodes.length);
      } else {
        location.radius = window.radiusObject.locationBaseRadius;
      }
    } else {
      if (location.isUnfolded) {
        location.radius =
          window.radiusObject.locationSmallOpenRadius *
          Math.sqrt(location.nodes.length);
      } else {
        location.radius = window.radiusObject.locationSmallBaseRadius;
      }
    }
  });
  nodes.forEach((node) => {
    if (node.isBSCC) {
      if (node.parentLocation.isUnfolded) {
        node.radius = window.radiusObject.nodeOpenRadius;
      } else {
        node.radius = window.radiusObject.nodeBaseRadius;
      }
    } else {
      if (node.parentLocation.isUnfolded) {
        node.radius = window.radiusObject.nodeSmallOpenRadius;
      } else {
        node.radius = window.radiusObject.nodeSmallBaseRadius;
      }
    }
  });

  return { nodes, locations, maxSdEdgeWeight, maxSdLocationWeight };
}

function _findMaximumSDWeights(locations) {
  let maxSdEdgeWeight = 0;
  let maxSdLocationWeight = 0;
  locations.forEach((location) => {
    location.neighborLocationsStationary.forEach((weight) => {
      maxSdEdgeWeight = Math.max(maxSdEdgeWeight, weight);
    });
    maxSdLocationWeight = Math.max(
      location.stationaryDistributionWeight,
      maxSdLocationWeight
    );
  });
  return { maxSdEdgeWeight, maxSdLocationWeight };
}

function _generateColors(count) {
  if (count < 1) {
    return [];
  }
  return iwanthue(
    count, // Colors
    {
      clustering: "force-vector",
      colorSpace: [150, 130, 4.75, 103.4, 62.99, 100],
      quality: 100,
      distance: "compromise",
    }
  );
}
