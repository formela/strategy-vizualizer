
export { drawConnections, drawForce };

// ++                     ++
// +++ Drawing functions +++
// ++                     ++

// normalizeStationaryDistribution: if true, the most traveled edge will have a weight of 1
function drawConnections(
  p5,
  locations,
  maxStationaryDistributionWeight = 1,
  edgeThreshold = 0,
  useStationaryDistribution = false,
  normalizeStationaryDistribution = false,
  showEdgeNumbers = false
) {
  // The following code draws connections between locations/nodes.
  // There are four configurations:
  //    1. C->C: Closed to closed (location to location)
  //    2. C->O: Closed to open (location to node)
  //    3. O->C: Open to closed (node to location)
  //    4. O->O: Open to open (node to node)
  locations.forEach((location) => {
    location.neighborLocations.forEach((weight, neighborLocation) => {
      if (!location.isUnfolded) {
        if (neighborLocation.isUnfolded) {
          // C->O
          location.neighborNodes.forEach((weight, neighborNode) => {
            if (neighborNode.parentLocation !== neighborLocation) return;
            let finalWeight = 0;
            if (useStationaryDistribution) {
              if (normalizeStationaryDistribution) {
                finalWeight =
                  location.neighborNodesStationary.get(neighborNode) /
                  maxStationaryDistributionWeight;
              } else {
                finalWeight =
                  location.neighborNodesStationary.get(neighborNode);
              }
            } else {
              finalWeight = weight / location.nodes.length;
            }
            if (finalWeight >= edgeThreshold) {
              drawConnection(
                p5,
                location,
                neighborNode,
                finalWeight,
                showEdgeNumbers,
                !(useStationaryDistribution && normalizeStationaryDistribution)
              );
            }
          });
        } else {
          // C->C
          let finalWeight = 0;
          if (useStationaryDistribution) {
            if (normalizeStationaryDistribution) {
              finalWeight =
                location.neighborLocationsStationary.get(neighborLocation) /
                maxStationaryDistributionWeight;
            } else {
              finalWeight =
                location.neighborLocationsStationary.get(neighborLocation);
            }
          } else {
            finalWeight = weight / location.nodes.length;
          }
          if (finalWeight >= edgeThreshold) {
            drawConnection(
              p5,
              location,
              neighborLocation,
              finalWeight,
              showEdgeNumbers,
              !(useStationaryDistribution && normalizeStationaryDistribution)
            );
          }
        }
      } else {
        location.nodes.forEach((node1) => {
          node1.neighborNodes.forEach((weight, node2) => {
            if (node2.parentLocation.isUnfolded) {
              // O->O
              let finalWeight = 0;
              if (useStationaryDistribution) {
                if (normalizeStationaryDistribution) {
                  finalWeight =
                    node1.neighborNodesStationary.get(node2) /
                    maxStationaryDistributionWeight;
                } else {
                  finalWeight = node1.neighborNodesStationary.get(node2);
                }
              } else {
                finalWeight = weight;
              }
              if (finalWeight >= edgeThreshold) {
                drawConnection(
                  p5,
                  node1,
                  node2,
                  finalWeight,
                  showEdgeNumbers,
                  !(
                    useStationaryDistribution && normalizeStationaryDistribution
                  )
                );
              }
            } else {
              // O->C
              let finalWeight = 0;
              if (useStationaryDistribution) {
                if (normalizeStationaryDistribution) {
                  finalWeight =
                    node1.neighborLocationsStationary.get(
                      node2.parentLocation
                    ) / maxStationaryDistributionWeight;
                } else {
                  finalWeight = node1.neighborLocationsStationary.get(
                    node2.parentLocation
                  );
                }
              } else {
                finalWeight = node1.neighborLocations.get(node2.parentLocation);
              }
              if (finalWeight >= edgeThreshold) {
                drawConnection(
                  p5,
                  node1,
                  node2.parentLocation,
                  finalWeight,
                  showEdgeNumbers,
                  !(
                    useStationaryDistribution && normalizeStationaryDistribution
                  )
                );
              }
            }
          });
        });
      }
    });
  });
}

function drawForce(p5, node, forceVector, forceColor) {
  p5.push();
  p5.stroke(forceColor); // Set the color of the arrow to red
  p5.fill(forceColor);
  p5.strokeWeight(2); // Set the thickness of the arrow

  let startPosition = node.position
    .copy()
    .add(forceVector.copy().setMag(node.radius)); // Offset the start of the arrow to the outside of the node

  let arrowEnd = startPosition.copy().add(forceVector); // Calculate the end point of the arrow
  p5.line(startPosition.x, startPosition.y, arrowEnd.x, arrowEnd.y); // Draw the arrow line
  p5.pop();

  drawArrowhead(p5, arrowEnd, forceVector.heading() - p5.PI, 10, forceColor);
}

function drawArrowhead(p5, position, heading, arrowSize, arrowColor, strokeWeight = 1) {
  p5.push();
  p5.stroke(arrowColor);
  p5.fill(arrowColor);
  p5.strokeWeight(strokeWeight);

  p5.translate(position.x, position.y);
  p5.rotate(heading);
  p5.triangle(-arrowSize / 2, 0, arrowSize / 2, arrowSize / 2, arrowSize / 2, -arrowSize / 2);
  p5.pop();
}

function drawConnection(
  p5,
  startNode,
  endNode,
  weight,
  drawWeight = true,
  percentageFormat = true
) {
  let lineColor = p5.color(0,0,0,p5.map(weight, 0, 1, 20, 255)); // Set the line color based on weight
  let lineWidth = p5.map(weight, 0, 1, 1, 5); // Map the weight to line width between 1 and 5

  if (!(startNode.isBSCC && endNode.isBSCC)) {
    lineColor = p5.color("#0000000f");
    lineWidth = 1;
  }

  if (percentageFormat) {
    weight = (weight * 100).toFixed(1) + "%";
  } else {
    weight = weight.toFixed(2);
  }

  const backgroundWidth = 10;

  const draw = (isBackground, lineColor) => {
    p5.noFill();
    p5.push();
    p5.stroke(lineColor);
    p5.strokeWeight(lineWidth + (isBackground ? backgroundWidth : 0));

    if (startNode === endNode) {
      // If the start and end nodes are the same, draw a loop
      let loopRadius = startNode.radius * 0.9;
      let startAngle = Math.acos(loopRadius / (2 * startNode.radius));
      let arrowEndPosition = p5
        .createVector(1, 0)
        .setMag(loopRadius)
        .setHeading(-startAngle);

      p5.translate(startNode.position.x, startNode.position.y);
      p5.rotate(p5.PI / 4);
      p5.translate(-startNode.radius, 0);
      p5.arc(0, 0, loopRadius * 2, loopRadius * 2, startAngle, -startAngle);

      drawArrowhead(
        p5,
        arrowEndPosition,
        -startAngle - p5.PI / 2 - p5.PI / 10,
        10,
        lineColor,
        isBackground ? backgroundWidth : 1
      );
      p5.rotate(-p5.PI / 4);
      if (drawWeight) {
        drawConnectionText(
          p5,
          weight,
          -1.3 * loopRadius,
          -1.3 * loopRadius,
          "#201F1D"
        );
      }
    } else {
      // Calculate the control points for the Bézier curve
      let distance = endNode.position.copy().sub(startNode.position);
      let controlPoint1 = startNode.position
        .copy()
        .add(
          distance
            .copy()
            .mult(0.4)
            .add(p5.createVector(distance.y, -distance.x).mult(0.1))
        );
      let controlPoint2 = startNode.position
        .copy()
        .add(
          distance
            .copy()
            .mult(0.6)
            .add(p5.createVector(distance.y, -distance.x).mult(0.1))
        );

      let textPosition = startNode.position.copy().add(
        distance
          .copy()
          .mult(0.5)
          .add(
            p5
              .createVector(distance.y, -distance.x)
              .setMag(20 + distance.mag() / 10)
          )
      );

      let startPosition = startNode.position
        .copy()
        .add(
          controlPoint1.copy().sub(startNode.position).setMag(startNode.radius)
        ); // Offset the start of the arrow to the outside of the node
      let endPosition = endNode.position
        .copy()
        .sub(endNode.position.copy().sub(controlPoint2).setMag(endNode.radius)); // Offset the end of the arrow to the outside of the node

      // Draw the Bézier curve
      p5.bezier(
        startPosition.x,
        startPosition.y,
        controlPoint1.x,
        controlPoint1.y,
        controlPoint2.x,
        controlPoint2.y,
        endPosition.x,
        endPosition.y
      );

      // Draw the arrowhead
      let controlPoint2ToEndPosition = endPosition.copy().sub(controlPoint2);
      let arrowHeading = controlPoint2ToEndPosition.heading() - p5.PI;
      let arrowSize = p5.min(10, distance.mag());

      drawArrowhead(p5, endPosition, arrowHeading, arrowSize, lineColor, isBackground ? backgroundWidth : 1);

      if (drawWeight && startNode.isBSCC && endNode.isBSCC) {
        drawConnectionText(p5, weight, textPosition.x, textPosition.y, "#201F1D");
      }
    }
    
    p5.pop();
  }

  // p5.drawingContext.filter = 'blur(10px)';
  draw(true, "#FFFFFF44");
  // p5.drawingContext.filter = 'none';
  draw(false, lineColor);
}

function drawConnectionText(
  p5,
  content,
  x = 0,
  y = 0,
  fontColor = "#000000",
  size = 12
) {
  p5.push();
  p5.stroke(fontColor);
  p5.fill(fontColor);
  p5.strokeWeight(0.5);
  p5.textSize(size);
  p5.textAlign(p5.CENTER, p5.CENTER);
  p5.text(content, x, y);
  p5.pop();
}
