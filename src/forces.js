import { drawForce } from "./draw.js";

export {
  applyAttractionForce,
  applyRepulsionForce,
  applyGravityForce,
  applyAxialForce,
};

// ++                   ++
// +++ Force functions +++
// ++                   ++

// Implementation of ForceAtlas2: https://doi.org/10.1371/journal.pone.0098679

function applyAttractionForce(
  p5,
  node1,
  node2,
  weightImportance = 1,
  debug = false,
  edgeThreshold = 0,
  useStationaryDistribution = false,
  stationaryDistributionMaxWeight = 1
) {
  // Attraction force is directly proportional to the distance between the nodes
  // and the weight of the connection.
  // Significance of the weight is controlled by parameter weightImportance
  // useStationaryDistribution is a boolean that switches between transition weights and stationary distribution weights
  if (
    (node1.constructor.name === "Node" &&
      node2.constructor.name === "Node" &&
      !node1.neighborNodes.has(node2) &&
      !node2.neighborNodes.has(node1)) ||
    (node1.constructor.name === "Location" &&
      node2.constructor.name === "Location" &&
      !node1.neighborLocations.has(node2) &&
      !node2.neighborLocations.has(node1)) ||
    (node1.constructor.name === "Node" &&
      node2.constructor.name === "Location" &&
      !node1.neighborLocations.has(node2) &&
      !node2.neighborNodes.has(node1)) ||
    (node1.constructor.name === "Location" &&
      node2.constructor.name === "Node" &&
      !node1.neighborNodes.has(node2) &&
      !node2.neighborLocations.has(node1))
  ) {
    return;
  }

  // Get weight of the connections, or 0 if there is no connection.
  // If useStationaryDistribution is true, use the stationary distribution weights,
  // otherwise use the transition weights.
  // Divide the weight by the number of nodes in the location to normalize the weight
  let weight = 0;
  if (node1.constructor.name === "Node" && node2.constructor.name === "Node") {
    if (useStationaryDistribution) {
      weight = p5.max(
        node1.neighborNodes.get(node2) * node1.stationaryDistributionWeight ||
          0,
        node2.neighborNodes.get(node1) * node2.stationaryDistributionWeight || 0
      );
    } else {
      weight = p5.max(
        node1.neighborNodes.get(node2) || 0,
        node2.neighborNodes.get(node1) || 0
      );
    }
  } else if (
    node1.constructor.name === "Location" &&
    node2.constructor.name === "Location"
  ) {
    if (useStationaryDistribution) {
      weight = p5.max(
        node1.neighborLocationsStationary.get(node2) || 0,
        node2.neighborLocationsStationary.get(node1) || 0
      );
    } else {
      weight = p5.max(
        node1.neighborLocations.get(node2) / node1.nodes.length || 0,
        node2.neighborLocations.get(node1) / node2.nodes.length || 0
      );
    }
  } else if (
    node1.constructor.name === "Node" &&
    node2.constructor.name === "Location"
  ) {
    if (useStationaryDistribution) {
      weight = p5.max(
        node1.neighborLocations.get(node2) *
          node1.stationaryDistributionWeight || 0,
        node2.neighborNodesStationary.get(node1) || 0
      );
    } else {
      weight = p5.max(
        node1.neighborLocations.get(node2) || 0,
        node2.neighborNodes.get(node1) / node2.nodes.length || 0
      );
    }
  } else if (
    node1.constructor.name === "Location" &&
    node2.constructor.name === "Node"
  ) {
    if (useStationaryDistribution) {
      weight = p5.max(
        node1.neighborNodesStationary.get(node2) || 0,
        node2.neighborLocations.get(node1) *
          node2.stationaryDistributionWeight || 0
      );
    } else {
      weight = p5.max(
        node1.neighborNodes.get(node2) / node1.nodes.length || 0,
        node2.neighborLocations.get(node1) || 0
      );
    }
  }

  if (useStationaryDistribution) {
    weight /= stationaryDistributionMaxWeight;
  }

  if (weight < edgeThreshold) return;

  let distance = node1.position.copy().sub(node2.position);
  if (distance.mag() === 0) {
    // If the nodes are at the same position, add a small random offset
    distance = p5.createVector(p5.random(-1, 1), p5.random(-1, 1)).setMag(5);
  }
  node2.force.add(distance.copy().mult(weight ** weightImportance));
  node1.force.sub(distance.copy().mult(weight ** weightImportance));

  if (debug) {
    drawForce(
      p5,
      node1,
      distance.copy().mult(-(weight ** weightImportance)),
      "#437742"
    );
    drawForce(
      p5,
      node2,
      distance.copy().mult(weight ** weightImportance),
      "#437742"
    );
  }

  return distance
    .copy()
    .mult(weight ** weightImportance)
    .mag();
}

function applyRepulsionForce(
  p5,
  node1,
  node2,
  k = 1,
  debug = false,
  firstOnly = false,
  scaleByRadius = false
) {
  // Repulsion force is inversely proportional to the distance
  // and dependent on the degree (number of neighbors) of the nodes
  // F_r = k_r * (deg(node1)+1)*(deg(node2)+1) / d(node1, node2)
  let distance = node1.position.copy().sub(node2.position);
  if (distance.mag() === 0) {
    // If the nodes are at the same position, add a small random offset
    distance = p5.createVector(p5.random(-1, 1), p5.random(-1, 1)).setMag(5);
  }
  // let forceMag =
  //   (k * (node1.neighbors.size + 1) * (node2.neighbors.size + 1)) /
  //   distance.mag();
  let forceMag = k / distance.mag(); // Repulsion force dependent on distance only
  if (scaleByRadius) {
    forceMag *= node1.radius * node2.radius;
  }
  node1.force.add(distance.setMag(forceMag));
  if (!firstOnly) {
    node2.force.sub(distance.setMag(forceMag));
  }

  if (debug) {
    drawForce(p5, node1, distance, "#e2625e");
    if (!firstOnly) {
      drawForce(p5, node2, distance.copy().mult(-1), "#e2625e");
    }
  }

  return forceMag;
}

function applyGravityForce(
  p5,
  node,
  center,
  g = 1,
  debug = false,
  scaleByRadius = false
) {
  // Gravity force is proportional to the distance from the center
  // F_g = k_g * (deg(node)+1) / d(node, center)
  let distance = center.copy().sub(node.position).mult(g);
  if (scaleByRadius) {
    distance.setMag(distance.mag() * node.radius);
  }
  node.force.add(distance);

  if (debug) {
    drawForce(p5, node, distance, "#064f6e");
  }
  return distance.mag();
}

function applyAxialForce(
  p5,
  node1,
  node2,
  k = 1,
  debug = false,
  edgeThreshold = 0
) {
  // Axial force is applied to nodes in the unfolded locations.
  // It is meant to drag the nodes towards the strongest connected edges.
  // The force is perpendicular to the line between the node and the center of the location.
  // The magnitude of the force is proportional to the angle between the two lines.
  // - One is the line between the node and the center of the location.
  // - The other is the connecting edge.
  //
  // node1 is the node that is being dragged
  // node2 is either a node or a location that is connected to node1

  if (
    (node2.constructor.name === "Node" && !node1.neighborNodes.has(node2)) ||
    (node2.constructor.name === "Location" &&
      !node1.neighborLocations.has(node2))
  ) {
    return;
  }

  let weight = 0;
  if (node2.constructor.name === "Node") {
    weight = node1.neighborNodes.get(node2);
  } else if (node2.constructor.name === "Location") {
    weight = node1.neighborLocations.get(node2);
  }

  if (weight < edgeThreshold) return;

  const node1ToCenter = node1.position
    .copy()
    .sub(node1.parentLocation.position);
  const force1Direction = node1ToCenter
    .copy()
    .rotate(p5.PI / 2)
    .normalize();
  const node1ToNode2 = node2.position.copy().sub(node1.position);

  const dotProduct1 = force1Direction.dot(node1ToNode2);
  force1Direction.setMag(dotProduct1 * k * weight);

  node1.force.add(force1Direction);

  if (debug) {
    drawForce(p5, node1, force1Direction, "#00c6c0");
  }

  if (node2.constructor.name === "Node") {
    const node2ToCenter = node2.position
      .copy()
      .sub(node2.parentLocation.position);
    const force2Direction = node2ToCenter
      .copy()
      .rotate(p5.PI / 2)
      .normalize();
    const dotProduct2 = force2Direction.dot(node1ToNode2.copy().mult(-1));
    force2Direction.setMag(dotProduct2 * k * weight);
    node2.force.add(force2Direction);

    if (debug) {
      drawForce(p5, node2, force2Direction, "#00c6c0");
    }
  }
}
