export { getIdFromQueryString, loadBackendExperiment };

// Check that backend is connected
async function _checkBackend(backendUrl) {
  let response;
  try {
    response = await fetch(backendUrl);
    console.log(response);
  } catch (error) {
    return false;
  }
  return response.ok;
}

// Get strategy id from query string
function getIdFromQueryString() {
  const urlParams = new URLSearchParams(window.location.search);
  if (!urlParams.has("id")) {
    return null;
  }
  return {
    id: urlParams.get("id"),
    url: urlParams.get("url"),
  };
}

// Get a strategy fom the backend
async function _getStrategy(backendUrl, strategyId) {
  let response;
  try {
    response = await fetch(`${backendUrl}/api/${strategyId}`);
  } catch (error) {
    return null;
  }
  if (!response.ok) {
    return null;
  }
  return response.json();
}

async function loadBackendExperiment(backendUrl, urlFromQueryString = true) {
  const strategyInfo = getIdFromQueryString();
  if (strategyInfo.url && urlFromQueryString) {
    backendUrl = strategyInfo.url;
  }
  const backendOk = await _checkBackend(backendUrl);
  if (!strategyInfo || !backendOk) {
    return null;
  }
  const strategy = await _getStrategy(backendUrl, strategyInfo.id);
  return strategy;
}
