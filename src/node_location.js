
export { Location, Node };

// ++              ++
// +++ Node class +++
// ++              ++

class Node {
  constructor(p5, position, color, name, displayName = "") {
    this.position = position;
    this.force = p5.createVector(0, 0);
    // Each neighbor is stored as a map entry with the end node as key and the weight as value.
    this.neighborNodes = new Map();
    this.neighborLocations = new Map();
    this.neighborNodesStationary = new Map();
    this.neighborLocationsStationary = new Map();
    this.parentLocation = null;
    this.radius = window.radiusObject.nodeBaseRadius;
    this.color = color;
    this.stationaryDistributionWeight = 1;
    this.name = name;
    this.displayName = displayName;
    this.hovering = false;
    this.highlighted = false;
    this.isBSCC = true;
  }

  findNeighborLocations() {
    this.neighborLocations = new Map();

    // Go through all neighbors of the nodes and add their locations to the neighbors map
    this.neighborNodes.forEach((weight, node) => {
      if (this.neighborLocations.has(node.parentLocation)) {
        let newWeight =
          this.neighborLocations.get(node.parentLocation) + weight;
        this.neighborLocations.set(node.parentLocation, newWeight);
      } else {
        this.neighborLocations.set(node.parentLocation, weight);
      }
    });
  }

  update(p5, draggingElement, simulationStep) {
    if (this.parentLocation != null && this.parentLocation.isUnfolded) {
      if (this === draggingElement) {
        this.force = p5.createVector(0, 0);
      }
      this.position.add(this.force.mult(simulationStep));
      this.force = p5.createVector(0, 0);
    }
  }

  draw(
    p5,
    stationaryDistributionGradient,
    showStationaryState,
    maxSdLocationWeight,
    selectedElement
  ) {
    p5.push();
    if ((this.hovering || this.highlighted) && this.parentLocation.isUnfolded) {
      drawHighlightedOutline(p5, this);
    }

    // Choose fill
    if (showStationaryState) {
      p5.fill(
        stationaryDistributionGradient(
          this.stationaryDistributionWeight / maxSdLocationWeight
        ).hex()
      );
    } else {
      p5.fill(this.color);
    }

    p5.strokeWeight(2);
    p5.ellipse(this.position.x, this.position.y, this.radius * 2);
    if (selectedElement === this) {
      drawSelectedOutline(p5, this);
    }
    if (!this.parentLocation.isUnfolded) return;

    p5.textAlign(p5.CENTER, p5.CENTER);
    if (showStationaryState) {
      p5.fill(
        this.stationaryDistributionWeight / maxSdLocationWeight > 0.6 ? 255 : 0
      );
    } else {
      p5.fill(0);
    }
    if (this.hovering || this.highlighted) {
      p5.text(this.name, this.position.x, this.position.y);
    } else if (showStationaryState) {
      p5.text(
        (this.stationaryDistributionWeight * 100).toFixed(1) + "%",
        this.position.x,
        this.position.y
      );
    }

    p5.pop();
  }

  normalizeEdgeWeights() {
    let sumWeights = 0;
    this.neighborNodes.forEach((weight, _) => {
      sumWeights += weight;
    });
    this.neighborNodes.forEach((weight, node) => {
      this.neighborNodes.set(node, weight / sumWeights);
    });
  }

  addNeighbor(neighborNode, weight) {
    this.neighborNodes.set(neighborNode, weight);
  }

  recomputeStationaryConnections() {
    this.neighborNodesStationary = new Map();
    this.neighborLocationsStationary = new Map();

    this.neighborNodes.forEach((weight, neighborNode) => {
      this.neighborNodesStationary.set(
        neighborNode,
        weight * this.stationaryDistributionWeight
      );
    });

    this.neighborLocations.forEach((weight, neighborLocation) => {
      this.neighborLocationsStationary.set(
        neighborLocation,
        weight * this.stationaryDistributionWeight
      );
    });
  }
}

// #endregion

// ++                  ++
// +++ Location class +++
// ++                  ++
// #region

class Location {
  constructor(p5, position, color, name, displayName = "", lockedPosition = null) {
    this.position = position;
    this.lockedPosition = lockedPosition;
    this.force = p5.createVector(0, 0);
    this.nodes = [];
    this.neighborLocations = new Map();
    this.neighborNodes = new Map();
    this.neighborLocationsStationary = new Map();
    this.neighborNodesStationary = new Map();
    this.isUnfolded = false;
    this.radius = window.radiusObject.locationBaseRadius;
    this.nodesAngle = p5.random(0, 2 * p5.PI);
    this.color = color;
    this.stationaryDistributionWeight = 1;
    this.name = name;
    this.displayName = displayName;
    this.hovering = false;
    this.highlighted = false;
    this.isBSCC = true;
  }

  addNode(node) {
    this.nodes.push(node);
    node.parentLocation = this;
  }

  removeNode(node) {
    this.nodes = this.nodes.filter((n) => n !== node);
    node.parentLocation = null;
  }

  findNeighbors() {
    this.neighborNodes = new Map();
    this.neighborLocations = new Map();

    // Find neighbor nodes
    this.nodes.forEach((node) => {
      node.neighborNodes.forEach((weightConnection, neighborNode) => {
        if (neighborNode.parentLocation != this) {
          if (this.neighborNodes.has(neighborNode)) {
            let newWeight =
              this.neighborNodes.get(neighborNode) + weightConnection;
            this.neighborNodes.set(neighborNode, newWeight);
          } else {
            this.neighborNodes.set(neighborNode, weightConnection);
          }
        }
      });

      // Find neighbor locations
      node.neighborNodes.forEach((weightConnection, neighborNode) => {
        if (this.neighborLocations.has(neighborNode.parentLocation)) {
          let newWeight =
            this.neighborLocations.get(neighborNode.parentLocation) +
            weightConnection;
          this.neighborLocations.set(neighborNode.parentLocation, newWeight);
        } else {
          this.neighborLocations.set(
            neighborNode.parentLocation,
            weightConnection
          );
        }
      });
    });
  }

  recomputeStationaryConnections() {
    this.stationaryDistributionWeight = 0;
    this.nodes.forEach((node) => {
      this.stationaryDistributionWeight += node.stationaryDistributionWeight;
    });

    this.neighborNodesStationary = new Map();
    this.neighborLocationsStationary = new Map();

    this.nodes.forEach((node) => {
      node.neighborNodes.forEach((weight, neighborNode) => {
        if (this.neighborNodesStationary.has(neighborNode)) {
          let newWeight =
            this.neighborNodesStationary.get(neighborNode) +
            weight * node.stationaryDistributionWeight;
          this.neighborNodesStationary.set(neighborNode, newWeight);
        } else {
          this.neighborNodesStationary.set(
            neighborNode,
            weight * node.stationaryDistributionWeight
          );
        }
      });
    });

    this.nodes.forEach((node) => {
      node.neighborLocations.forEach((weight, neighborLocation) => {
        if (this.neighborLocationsStationary.has(neighborLocation)) {
          let newWeight =
            this.neighborLocationsStationary.get(neighborLocation) +
            weight * node.stationaryDistributionWeight;
          this.neighborLocationsStationary.set(neighborLocation, newWeight);
        } else {
          this.neighborLocationsStationary.set(
            neighborLocation,
            weight * node.stationaryDistributionWeight
          );
        }
      });
    });
  }

  setUnfold(unfolded) {
    this.isUnfolded = unfolded;
    if (unfolded) {
      if (this.isBSCC) {
        this.radius =
          window.radiusObject.locationOpenRadius * Math.sqrt(this.nodes.length);
      } else {
        this.radius =
          window.radiusObject.locationSmallOpenRadius *
          Math.sqrt(this.nodes.length);
      }
      this.nodes.forEach((node) => {
        if (node.isBSCC) {
          node.radius = window.radiusObject.nodeOpenRadius;
        } else {
          node.radius = window.radiusObject.nodeSmallOpenRadius;
        }
      });
    } else {
      if (this.isBSCC) {
        this.radius = window.radiusObject.locationBaseRadius;
      } else {
        this.radius = window.radiusObject.locationSmallBaseRadius;
      }
      this.nodes.forEach((node) => {
        if (node.isBSCC) {
          node.radius = window.radiusObject.nodeBaseRadius;
        } else {
          node.radius = window.radiusObject.nodeSmallBaseRadius;
        }
      });
    }
  }

  update(p5, draggingElement, simulationStep, background) {
    if (this.lockedPosition && background) {
      // Calculate the position on the canvas from the relative [0,1] locations stored in lockedPosition
      let aspectRatio = background.width / background.height;
      let canvasAspectRatio = p5.width / p5.height;
      let width = p5.width;
      let height = p5.height;
      if (aspectRatio > canvasAspectRatio) {
        width = p5.width;
        height = p5.width / aspectRatio;
      } else {
        width = p5.height * aspectRatio;
        height = p5.height;
      }

      this.position = p5.createVector(
        this.lockedPosition.x * width - width / 2,
        this.lockedPosition.y * height - height / 2
      );
    } else {
      if (this === draggingElement) {
        this.force = p5.createVector(0, 0);
      }
      this.position.add(this.force.mult(simulationStep));
    }

    this.force = p5.createVector(0, 0);

    // Put the nodes around the location if the location is folded
    if (!this.isUnfolded) {
      let angle = this.nodesAngle;
      let angleStep = (2 * p5.PI) / this.nodes.length;
      this.nodes.forEach((node) => {
        let nodePosition = p5.createVector(
          this.position.x + (this.radius - 1) * p5.cos(angle),
          this.position.y + (this.radius - 1) * p5.sin(angle)
        );
        node.position = nodePosition;
        angle += angleStep;
      });
      this.nodesAngle = (this.nodesAngle + 0.002) % (2 * p5.PI);
    }
  }

  draw(
    p5,
    stationaryDistributionGradient,
    showStationaryState,
    maxSdLocationWeight,
    selectedElement
  ) {
    if (this.isUnfolded) {
      p5.push();
      if (this.hovering || this.highlighted) {
        drawHighlightedOutline(p5, this);
      }
      if (showStationaryState) {
        p5.fill(
          stationaryDistributionGradient(
            this.stationaryDistributionWeight / maxSdLocationWeight
          ).hex()
        );
      } else {
        p5.fill(this.color);
      }
      p5.strokeWeight(2);
      p5.ellipse(this.position.x, this.position.y, this.radius * 2);
      if (selectedElement === this) {
        drawSelectedOutline(p5, this);
      }

      p5.textAlign(p5.CENTER, p5.CENTER);
      if (showStationaryState) {
        p5.fill(
          this.stationaryDistributionWeight / maxSdLocationWeight > 0.6
            ? 255
            : 0
        );
      } else {
        p5.fill(0);
      }
      if (this.hovering || this.highlighted) {
        if (this.displayName !== "") {
          p5.textStyle(p5.BOLD);
          p5.text(this.displayName, this.position.x, this.position.y - 10);
          p5.textStyle(p5.NORMAL);
          p5.text(this.name, this.position.x, this.position.y + 10);
        } else {
          p5.text(this.name, this.position.x, this.position.y);
        }
      } else if (showStationaryState) {
        p5.text(
          (this.stationaryDistributionWeight * 100).toFixed(1) + "%",
          this.position.x,
          this.position.y
        );
      }

      p5.pop();

      this.nodes.forEach((node) => {
        node.draw(
          p5,
          stationaryDistributionGradient,
          showStationaryState,
          maxSdLocationWeight,
          selectedElement
        );
      });
    } else {
      this.nodes.forEach((node) => {
        node.draw(
          p5,
          stationaryDistributionGradient,
          showStationaryState,
          maxSdLocationWeight,
          selectedElement
        );
      });
      p5.push();
      if (this.hovering || this.highlighted) {
        drawHighlightedOutline(p5, this);
      }
      if (showStationaryState) {
        p5.fill(
          stationaryDistributionGradient(
            this.stationaryDistributionWeight / maxSdLocationWeight
          ).hex()
        );
      } else {
        p5.fill(this.color);
      }
      p5.strokeWeight(2);
      p5.ellipse(this.position.x, this.position.y, this.radius * 2);
      if (selectedElement === this) {
        drawSelectedOutline(p5, this);
      }

      p5.textAlign(p5.CENTER, p5.CENTER);
      if (showStationaryState) {
        p5.fill(
          this.stationaryDistributionWeight / maxSdLocationWeight > 0.6
            ? 255
            : 0
        );
      } else {
        p5.fill(0);
      }
      if (this.hovering || this.highlighted) {
        if (this.displayName !== "") {
          p5.textStyle(p5.BOLD);
          p5.text(this.displayName, this.position.x, this.position.y - 10);
          p5.textStyle(p5.NORMAL);
          p5.text(this.name, this.position.x, this.position.y + 10);
        } else {
          p5.text(this.name, this.position.x, this.position.y);
        }
      } else if (showStationaryState) {
        p5.text(
          (this.stationaryDistributionWeight * 100).toFixed(1) + "%",
          this.position.x,
          this.position.y
        );
      }

      p5.pop();
    }
  }
}

function drawSelectedOutline(p5, element) {
  p5.push();
  p5.noFill();
  p5.stroke(0, 0, 0, 240);
  p5.ellipse(element.position.x, element.position.y, (element.radius + 2) * 2);
  p5.stroke(0, 0, 0, 160);
  p5.ellipse(element.position.x, element.position.y, (element.radius + 4) * 2);
  p5.stroke(0, 0, 0, 80);
  p5.ellipse(element.position.x, element.position.y, (element.radius + 6) * 2);
  p5.noStroke();
  p5.pop();
}

function drawHighlightedOutline(p5, element) {
  p5.push();
  p5.noFill();
  p5.stroke(0, 0, 0, 240);
  p5.ellipse(element.position.x, element.position.y, (element.radius + 2) * 2);
  p5.stroke(0, 0, 0, 160);
  p5.ellipse(element.position.x, element.position.y, (element.radius + 4) * 2);
  p5.stroke(0, 0, 0, 80);
  p5.ellipse(element.position.x, element.position.y, (element.radius + 6) * 2);
  p5.noStroke();
  p5.pop();
}
