export { Agent, initAgents };

class Agent {
  constructor(p5, position, color) {
    this.position = position;
    this.force = p5.createVector(0, 0);
    this.radius = window.radiusObject.agentManyRadius;
    this.color = color || p5.color("#FFA70F");
    this.snapshotParents = [];
  }

  update(simulationStep) {
    this.position.add(this.force.mult(simulationStep));
    this.force.mult(0);
  }

  draw(p5) {
    p5.push();
    p5.strokeWeight(1);
    p5.stroke(0);
    p5.fill(this.color);
    p5.ellipse(this.position.x, this.position.y, this.radius * 2);
    p5.pop();
  }
}

function initAgents(p5, agentsCount, parent, snapshotsCount) {
  if (parent.neighborNodes.size === 0) return [];
  let agents = [...Array(agentsCount).keys()].map(() => {
    return new Agent(
      p5,
      parent.position
        .copy()
        .add(p5.createVector(p5.random(-5, 5), p5.random(-5, 5)))
    );
  });
  agents.forEach((agent) => {
    agent.snapshotParents.push(parent);
    for (let i = 1; i < snapshotsCount; i++) {
      let items = Array.from(agent.snapshotParents[i - 1].neighborNodes.keys());
      let weights = Array.from(
        agent.snapshotParents[i - 1].neighborNodes.values()
      );

      agent.snapshotParents.push(_weightedRandom(items, weights));
    }
  });
  return agents;
}

function _weightedRandom(items, weights) {
  var i;

  for (i = 1; i < weights.length; i++) weights[i] += weights[i - 1];

  var random = Math.random() * weights[weights.length - 1];

  for (i = 0; i < weights.length; i++) if (weights[i] > random) break;

  return items[i];
}
