import { Matrix, solve } from "ml-matrix";

import { re } from "mathjs";

export {
  generateStateOverTime,
  nextStateFromTransitionMatrix,
  stationaryDistributionFromNodes,
};

function _transitionMatrixFromNodes(nodes) {
  // Create a square matrix with dimensions equal to the number of nodes
  // Fill the matrix with the weights of the connections
  // Return array of arrays representing the matrix
  let matrix = [...Array(nodes.length).keys()].map((i) =>
    [...Array(nodes.length).keys()].map((j) => 0)
  );
  nodes.forEach((node1, i) => {
    nodes.forEach((node2, j) => {
      if (node1.neighborNodes.has(node2)) {
        matrix[j][i] = node1.neighborNodes.get(node2);
      }
    });
  });
  return matrix;
}

// function _transitionMatrixFromLocations(locations) {
//   // Create a square matrix with dimensions equal to the number of locations
//   // Fill the matrix with the weights of the connections
//   // Return array of arrays representing the matrix
//   let matrix = [...Array(locations.length).keys()].map((i) =>
//     [...Array(locations.length).keys()].map((j) => 0)
//   );
//   locations.forEach((node1, i) => {
//     locations.forEach((node2, j) => {
//       if (node1.neighborLocations.has(node2)) {
//         matrix[j][i] = node1.neighborLocations.get(node2);
//       }
//     });
//   });
//   return matrix;
// }

function stationaryDistributionFromNodes(nodes) {
  // Calculate the stationary distribution of the Markov chain
  let inputMatrix = _transitionMatrixFromNodes(nodes);
  inputMatrix.push(inputMatrix[0].map((i) => 1));
  let matrix = new Matrix(inputMatrix);
  matrix = Matrix.sub(matrix, Matrix.eye(matrix.rows, matrix.columns));
  let b = Matrix.zeros(matrix.rows, 1);
  b.set(matrix.rows - 1, 0, 1);
  let x = solve(matrix, b);
  return x.to1DArray();
}

// function stationaryDistributionFromLocations(locations) {
//   // Calculate the stationary distribution of the Markov chain
//   let inputMatrix = _transitionMatrixFromLocations(locations);
//   inputMatrix.push(inputMatrix[0].map((i) => 1));
//   let matrix = new Matrix(inputMatrix);
//   matrix = Matrix.sub(matrix, Matrix.eye(matrix.rows, matrix.columns));
//   let b = Matrix.zeros(matrix.rows, 1);
//   b.set(matrix.rows - 1, 0, 1);
//   let x = solve(matrix, b);
//   return x.to1DArray();
// }

function nextStateFromTransitionMatrix(matrix, currentState) {
  // Calculate the next state of the Markov chain given the transition matrix and the current state
  let next = matrix.map((row) =>
    row.reduce((acc, val, i) => acc + val * currentState[i], 0)
  );
  return next;
}

// This function creates the state of the system over time.
// It takes an origin, where the guard starts with 100% probability.
// Then it calculates the next state using the transition matrix, so the probability disperses to the next nodes.
// It repeats this process for a number of iterations.
// The result is an array, where the values are arrays of probabilities over time.
// If the origin is a location, it will start with 1/n probability for each of the child nodes, where n is the number of child nodes.
function generateStateOverTime(nodes, origin, iterations) {
  let transitionMatrix = _transitionMatrixFromNodes(nodes);
  let state = Array(nodes.length).fill(0);

  if (origin.constructor.name === "Location") {
    nodes.forEach((node, i) => {
      if (origin.nodes.indexOf(node) !== -1) {
        state[i] = 1 / origin.nodes.length;
      }
    });
  } else {
    nodes.forEach((node, i) => {
      if (node === origin) {
        state[i] = 1;
      }
    });
  }

  let result = [];
  nodes.forEach((_, i) => {
    result[i] = Array(iterations).fill(0);
    result[i][0] = state[i];
  });

  for (let i = 1; i < iterations; i++) {
    state = nextStateFromTransitionMatrix(transitionMatrix, state);
    nodes.forEach((_, j) => {
      result[j][i] = state[j];
    });
  }
  return result;
}
