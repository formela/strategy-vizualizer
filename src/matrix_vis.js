import chroma from "chroma-js";

export { MatrixVisualization };

class MatrixVisualization {
  constructor(locations, nodes, size = 100) {
    this.size = size;
    this.cellSize = Math.floor((size - 10) / nodes.length);
    this.matrixWidth = this.cellSize * nodes.length;
    this.matrixHeight = this.cellSize * nodes.length;
    this.margin = size - this.cellSize * nodes.length;
    this.startContent = []; // Contains the nodes and locations on the left side of the matrix
    this.endContent = []; // Contains the nodes and locations on the top side of the matrix
    for (let location of locations) {
      for (let node of location.nodes) {
        this.startContent.push({
          node: node,
          location: location,
        });
        this.endContent.push({
          node: node,
          location: location,
        });
      }
    }
  }

  draw(p5) {
    p5.push();
    p5.strokeWeight(0);

    const mouseIndex = this.getCellAt(p5.mouseX, p5.mouseY);
    const mouseI = mouseIndex[0];
    const mouseJ = mouseIndex[1];
    const cell = this.getCellContent(mouseI, mouseJ);
    const scale = chroma.scale(["#ddd", "#000"]).mode("lab").gamma(2);

    // Draw the axes
    for (let k = 0; k < this.startContent.length; k++) {
      if (mouseI === k) {
        p5.fill(this.endContent[k].node.color);
      } else {
        p5.fill(this.endContent[k].location.color);
      }
      p5.rect(k * this.cellSize + this.margin, 0, this.cellSize, this.margin);
      if (mouseJ === k) {
        p5.fill(this.startContent[k].node.color);
      } else {
        p5.fill(this.startContent[k].location.color);
      }
      p5.rect(0, k * this.cellSize + this.margin, this.margin, this.cellSize);
    }

    // Draw the matrix
    for (let i = 0; i < this.endContent.length; i++) {
      this.endContent[i].node.highlighted = false;
      this.endContent[i].location.highlighted = false;
      for (let j = 0; j < this.startContent.length; j++) {
        let weight = this.endContent[i].node.neighborNodes.get(
          this.startContent[j].node
        );
        if (weight !== undefined) {
          // p5.fill(p5.map(weight, 0, 1, 200, 0));
          p5.fill(scale(weight).hex());
        } else if (
          (this.endContent[i].node === cell.endNode &&
            this.startContent[j].node !== cell.startNode) ||
          this.endContent[i].node.hovering
        ) {
          p5.fill(this.endContent[i].node.color);
        } else if (
          (this.startContent[j].node === cell.startNode &&
            this.endContent[i].node !== cell.endNode) ||
          this.startContent[j].node.hovering
        ) {
          p5.fill(this.startContent[j].node.color);
        } else if (
          (this.endContent[i].location === cell.endLocation &&
            this.startContent[j].location !== cell.startLocation) ||
          this.endContent[i].location.hovering
        ) {
          p5.fill(this.endContent[i].location.color);
        } else if (
          (this.startContent[j].location === cell.startLocation &&
            this.endContent[i].location !== cell.endLocation) ||
          this.startContent[j].location.hovering
        ) {
          p5.fill(this.startContent[j].location.color);
        } else if (
          cell.startLocation &&
          cell.startLocation === cell.endLocation &&
          cell.startLocation === this.startContent[j].location
        ) {
          p5.fill(cell.startLocation.color);
        } else {
          p5.fill(255);
        }
        p5.rect(
          j * this.cellSize + this.margin,
          i * this.cellSize + this.margin,
          this.cellSize,
          this.cellSize
        );
      }
    }

    // Draw the lines in the table
    p5.noFill();
    p5.stroke("#eee");
    p5.strokeWeight(1);

    let k = 0;
    while (k < this.startContent.length) {
      let width = this.startContent[k].location.nodes.length;
      p5.rect(
        k * this.cellSize + this.margin,
        this.margin,
        this.cellSize * width,
        this.cellSize * this.startContent.length
      );
      p5.rect(
        this.margin,
        k * this.cellSize + this.margin,
        this.cellSize * this.startContent.length,
        this.cellSize * width
      );
      k += width;
    }

    p5.stroke("#777");

    k = 0;
    while (k < this.startContent.length) {
      let width = this.startContent[k].location.nodes.length;
      p5.rect(
        k * this.cellSize + this.margin,
        0,
        this.cellSize * width,
        this.margin
      );
      p5.rect(
        0,
        k * this.cellSize + this.margin,
        this.margin,
        this.cellSize * width
      );
      k += width;
    }

    // TODO: The coordinate system between x,y and i,j is broken
    // Draw the on-hover label
    if (mouseI >= 0 && mouseJ >= 0) {
      this._drawMatrixRectangle(p5, mouseI, mouseJ, 1, 1, null, 0);

      let weight = (cell.endNode.neighborNodes.get(cell.startNode) || 0) * 100;

      let xPosition =
        p5.mouseX + 15 < this.size - 120 ? p5.mouseX + 15 : p5.mouseX - 120;
      let yPosition =
        p5.mouseY + 50 < this.size - 50 ? p5.mouseY + 15 : p5.mouseY - 50;

      this._drawLabel(
        p5,
        xPosition,
        yPosition,
        120,
        50,
        `From: ${cell.endNode.name}\nTo: ${
          cell.startNode.name
        }\nWeight:${weight.toFixed(2)} %`
      );
    }

    if (mouseI < 0 && mouseJ >= 0) {
      let yPosition =
        p5.mouseY + 50 < this.size - 50 ? p5.mouseY + 15 : p5.mouseY - 50;
      this._drawLabel(
        p5,
        p5.mouseX + 15,
        yPosition,
        120,
        50,
        `Location: ${cell.endLocation.name}\nNode: ${cell.endNode.name}`
      );
    }
    if (mouseJ < 0 && mouseI >= 0) {
      let xPosition =
        p5.mouseX + 15 < this.size - 120 ? p5.mouseX + 15 : p5.mouseX - 120;
      this._drawLabel(
        p5,
        xPosition,
        p5.mouseY + 15,
        120,
        50,
        `Location: ${cell.startLocation.name}\nNode: ${cell.startNode.name}`
      );
    }

    if (mouseI >= 0) {
      if (cell.startNode) {
        cell.startNode.highlighted = true;
        cell.startLocation.highlighted = true;
      }
    }
    if (mouseJ >= 0) {
      if (cell.endNode) {
        cell.endNode.highlighted = true;
        cell.endLocation.highlighted = true;
      }
    }
    p5.pop();
  }

  doubleClicked(p5) {
    const mouseIndex = this.getCellAt(p5.mouseX, p5.mouseY);
    const mouseI = mouseIndex[0];
    const mouseJ = mouseIndex[1];
    const cell = this.getCellContent(mouseI, mouseJ);
    if (mouseI >= 0) {
      if (cell.startNode) {
        cell.startLocation.setUnfold(true);
      }
    }
    if (mouseJ >= 0) {
      if (cell.endNode) {
        cell.endLocation.setUnfold(true);
      }
    }
  }

  // Transforms screen coordinates to matrix coordinates
  getCellAt(x, y) {
    if (x < 0 || x > this.size || y < 0 || y > this.size) return [-1, -1];
    let i = Math.floor((x - this.margin) / this.cellSize);
    let j = Math.floor((y - this.margin) / this.cellSize);
    return [i, j];
  }

  getCellContent(j, i) {
    let cell = {
      endNode: null,
      endLocation: null,
      startNode: null,
      startLocation: null,
    };
    if (i >= 0) {
      cell.endNode = this.endContent[i].node;
      cell.endLocation = this.endContent[i].location;
    }
    if (j >= 0) {
      cell.startNode = this.startContent[j].node;
      cell.startLocation = this.startContent[j].location;
    }
    return cell;
  }

  _drawMatrixRectangle(p5, i, j, width, height, fill, stroke) {
    p5.push();
    if (fill === null || fill === undefined) {
      p5.noFill();
    } else {
      p5.fill(fill);
    }
    if (stroke === null || stroke === undefined) {
      p5.noStroke();
    } else {
      p5.stroke(stroke);
    }
    p5.rect(
      i * this.cellSize + this.margin,
      j * this.cellSize + this.margin,
      width * this.cellSize,
      height * this.cellSize
    );
    p5.pop();
  }

  _getLabelAlignment(p5, i, j) {
    let alignment = {
      i: p5.CENTER,
      j: p5.TOP,
    };
    if (i < this.startContent.length / 3) {
      alignment.i = p5.LEFT;
    } else if (i > (this.startContent.length * 2) / 3) {
      alignment.i = p5.RIGHT;
    }
    if (j > this.endContent.length / 2) {
      alignment.j = p5.BOTTOM;
    }
    return alignment;
  }

  _drawLabel(p5, x, y, width, height, text) {
    p5.push();

    // Draw a box around the text
    p5.strokeWeight(0);
    p5.fill(255, 200);
    p5.rect(x, y, width, height);

    // Draw the text
    p5.fill(0);
    p5.strokeWeight(0);
    p5.textAlign(p5.LEFT, p5.TOP);
    p5.text(text, x + 5, y + 5);
    p5.pop();
  }
}
