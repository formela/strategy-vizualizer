import {
  getIdFromQueryString,
  loadBackendExperiment,
} from "./src/backend_connect.js";
import { drawConnections, drawForce } from "./src/draw.js";
import {
  applyAttractionForce,
  applyAxialForce,
  applyGravityForce,
  applyRepulsionForce,
} from "./src/forces.js";

import chroma from "chroma-js";
import airportStrategy from "./data/airport.json";
import sourceGraph from "./data/office.json";
import officeNoMemoryStrategy from "./data/office_no_memory.json";
import sweepStrategy from "./data/sweep.json";
import { initAgents } from "./src/agents.js";
import { calculateBSCCs } from "./src/bscc.js";
import { loadJsonExperiment } from "./src/load.js";
import { generateStateOverTime } from "./src/markov_chain_utils.js";
import { MatrixVisualization } from "./src/matrix_vis.js";

var canvasWidth = 0;
var canvasHeight = 0;

var gravityFactor = 0.1;
var gravityIntralocationFactor = 1;
var repulsionFactor = 500;
var repulsionIntralocationFactor = 10000;
var weightFactor = 1;
var axialForceFactor = 0.1;
var simulationStep = 0.05;
var edgeThreshold = 0;

window.radiusObject = {
  nodeBaseRadius: 10,
  nodeOpenRadius: 20,
  nodeSmallBaseRadius: 6,
  nodeSmallOpenRadius: 8,
  locationBaseRadius: 24,
  locationOpenRadius: 36,
  locationSmallBaseRadius: 12,
  locationSmallOpenRadius: 18,
  agentManyRadius: 2,
  agentOneRadius: 8,
};

var draggingElement = null;
var selectedElement = null;
var elementInDetail = null;

var debug = false;
var enableForces = true;
var showEdgeNumbers = true;
var showStationaryState = false;
var normalizeStationaryDistribution = true;
var showAgents = false;
var showBSCC = true;
var showMaximumEdgeWeight = true;

var mouseClickPosition = [0, 0];

var nodes = [];
var locations = [];
var maxSdEdgeWeight = 0;
var maxSdLocationWeight = 0;
var background = undefined;
var zoomFactor = 1;
var panning = { x: 0, y: 0 };
const stationaryDistributionGradient = chroma.scale(["#fff", "#054A91"]);

const backendUrl = "http://localhost:5000";

var agents = [];
var manyAgents = true;
var agentsCount = 400;
var currentSnapshot = 0;
var snapshotsCount = 101;
var stateOverTime = [];

var matrixVisualization = null;

var visGraph = null;
if (getIdFromQueryString()) {
  visGraph = await loadBackendExperiment(backendUrl);
  if (!visGraph) {
    window.alert("Requested strategy not found.");
  }
}

const p5Graph = new p5((p5) => {
  p5.setup = function () {
    canvasWidth = p5.windowWidth;
    canvasHeight = p5.windowHeight - 64;
    const canvasGraph = p5.createCanvas(canvasWidth, canvasHeight);
    canvasGraph.style("display", "block");

    p5.frameRate(60);

    ({ nodes, locations, maxSdEdgeWeight, maxSdLocationWeight, background } =
      loadJsonExperiment(p5, visGraph ? visGraph : sourceGraph));
  };

  p5.draw = function () {
    p5.background("#FFFFFF");

    p5.translate(p5.width / 2, p5.height / 2);

    // Apply zoom and panning
    p5.translate(panning.x, panning.y);
    p5.scale(zoomFactor);

    //Draw background image
    if (background) {
      // Draw stretched blurred background
      // p5.image(background, 0, 0, p5.width, p5.height);
      // p5.background("#FFFFFF88");
      // p5.filter(p5.BLUR, 10);

      // Keep aspect ratio and center the image
      let aspectRatio = background.width / background.height;
      let canvasAspectRatio = p5.width / p5.height;
      let width = p5.width;
      let height = p5.height;
      if (aspectRatio > canvasAspectRatio) {
        width = p5.width;
        height = p5.width / aspectRatio;
      } else {
        width = p5.height * aspectRatio;
        height = p5.height;
      }
      
      p5.image(background, -width/2, -height/2, width, height);
      p5.background("#FFFFFF88");
    }

    if (enableForces) {
      applyForces(p5, locations);
    }

    if (showAgents) {
      applyAgentForces(p5, agents, currentSnapshot);
    }

    locations.forEach((location) => {
      location.update(p5, draggingElement, simulationStep, background);
    });
    nodes.forEach((node) => {
      node.update(p5, draggingElement, simulationStep);
    });
    if (showAgents) {
      agents.forEach((agent) => {
        agent.update(simulationStep);
      });
    }
    p5.push();
    locations.forEach((location) => {
      location.draw(
        p5,
        stationaryDistributionGradient,
        showStationaryState,
        maxSdLocationWeight,
        selectedElement,
        showBSCC
      );
    });
    p5.pop();

    drawConnections(
      p5,
      locations,
      maxSdEdgeWeight,
      edgeThreshold,
      showStationaryState,
      normalizeStationaryDistribution,
      showEdgeNumbers
    );

    if (showAgents) {
      agents.forEach((agent) => {
        agent.draw(p5);
      });
    }
  };

  p5.windowResized = function () {
    canvasWidth = p5.windowWidth;
    canvasHeight = p5.windowHeight - 84;
    p5.resizeCanvas(canvasWidth, canvasHeight);
  };

  // ++                     ++
  // +++ Mouse interaction +++
  // ++                     ++
  // #region

  p5.mousePressed = function (event) {
    p5.originalMouseX = p5.mouseX;
    p5.originalMouseY = p5.mouseY;

    if (!(event.buttons & 1)) {
      // Only left mouse button
      return;
    }

    mouseClickPosition = [p5.mouseX, p5.mouseY];
    for (const location of locations) {
      let d = p5.dist(
        (p5.mouseX - canvasWidth / 2 - panning.x) / zoomFactor,
        (p5.mouseY - canvasHeight / 2 - panning.y) / zoomFactor,
        location.position.x,
        location.position.y
      );

      if (d < location.radius) {
        draggingElement = location;
        p5.originalNodeX = location.position.x;
        p5.originalNodeY = location.position.y;
        break;
      }
    }
    for (const node of nodes) {
      let d = p5.dist(
        (p5.mouseX - canvasWidth / 2 - panning.x) / zoomFactor,
        (p5.mouseY - canvasHeight / 2 - panning.y) / zoomFactor,
        node.position.x,
        node.position.y
      );

      if (d < node.radius) {
        draggingElement = node;
        p5.originalNodeX = node.position.x;
        p5.originalNodeY = node.position.y;
        break;
      }
    }
    if (debug) console.log(draggingElement);
  };

  p5.mouseDragged = function (event) {
    if (draggingElement) {
      // Scale the dragging element position based on zoom factor
      draggingElement.position.x = p5.originalNodeX + (p5.mouseX - p5.originalMouseX) / zoomFactor;
      draggingElement.position.y = p5.originalNodeY + (p5.mouseY - p5.originalMouseY) / zoomFactor;
    }

    // On middle mouse button, pan the view
    if (event.buttons & 4) {
      panning.x += p5.mouseX - p5.originalMouseX;
      panning.y += p5.mouseY - p5.originalMouseY;
      p5.originalMouseX = p5.mouseX;
      p5.originalMouseY = p5.mouseY;
      return;
    }

    for (const location of locations) {
      let d = p5.dist(
        (p5.mouseX - canvasWidth / 2 - panning.x) / zoomFactor,
        (p5.mouseY - canvasHeight / 2 - panning.y) / zoomFactor,
        location.position.x,
        location.position.y
      );

      if (d < location.radius) {
        location.hovering = true;
      } else {
        location.hovering = false;
      }
    }

    for (const node of nodes) {
      let d = p5.dist(
        (p5.mouseX - canvasWidth / 2 - panning.x) / zoomFactor,
        (p5.mouseY - canvasHeight / 2 - panning.y) / zoomFactor,
        node.position.x,
        node.position.y
      );

      if (d < node.radius) {
        node.hovering = true;
      } else {
        node.hovering = false;
      }
    }
  };

  p5.mouseReleased = function () {
    if (event.target.id !== "defaultCanvas0") return;
    if (
      Math.abs(mouseClickPosition[0] - p5.mouseX) < 5 &&
      Math.abs(mouseClickPosition[1] - p5.mouseY) < 5
    ) {
      selectedElement = draggingElement;
      agents = [];
      document.getElementById("currentSnapshot").value = currentSnapshot;

      if (!selectedElement) {
        document.getElementById("agentPanel").style.display = "none";
        currentSnapshot = 0;
        hideDetailPanel();
        return;
      }

      if (
        selectedElement.constructor.name === "Node" &&
        !selectedElement.parentLocation.isUnfolded
      ) {
        selectedElement = selectedElement.parentLocation;
      }

      stateOverTime = generateStateOverTime(
        nodes,
        selectedElement,
        snapshotsCount
      );

      showDetailPanel(p5, selectedElement, true);

      if (selectedElement.constructor.name === "Location") {
        if (!manyAgents) agentsCount = selectedElement.nodes.length;
        for (const node of selectedElement.nodes) {
          agents = agents.concat(
            initAgents(
              p5,
              Math.floor(agentsCount / selectedElement.nodes.length),
              node,
              snapshotsCount
            )
          );
        }
      } else {
        if (!manyAgents) agentsCount = 1;
        agents = initAgents(p5, agentsCount, selectedElement, snapshotsCount);
      }

      if (!manyAgents) {
        agents.forEach((agent) => {
          agent.radius = window.radiusObject.agentOneRadius;
        });
      }

      document.getElementById("agentPanel").style.display = "block";

      if (debug) console.log("Selecting: " + selectedElement);
    }
    draggingElement = null;
  };

  p5.doubleClicked = function () {
    for (let i = 0; i < locations.length; i++) {
      let location = locations[i];
      let d = p5.dist(
        (p5.mouseX - canvasWidth / 2 - panning.x) / zoomFactor,
        (p5.mouseY - canvasHeight / 2 - panning.y) / zoomFactor,
        location.position.x,
        location.position.y
      );

      if (d < location.radius) {
        location.setUnfold(!location.isUnfolded);
        break;
      }
    }
  };

  // On hover, show the name of the node
  p5.mouseMoved = function () {
    let hovering = null;
    for (const location of locations) {
      let d = p5.dist(
        (p5.mouseX - canvasWidth / 2 - panning.x) / zoomFactor,
        (p5.mouseY - canvasHeight / 2 - panning.y) / zoomFactor,
        location.position.x,
        location.position.y
      );

      if (d < location.radius) {
        location.hovering = true;
        hovering = location;
      } else {
        location.hovering = false;
      }
    }

    for (const node of nodes) {
      let d = p5.dist(
        (p5.mouseX - canvasWidth / 2 - panning.x) / zoomFactor,
        (p5.mouseY - canvasHeight / 2 - panning.y) / zoomFactor,
        node.position.x,
        node.position.y
      );

      if (d < node.radius && node.parentLocation.isUnfolded) {
        node.hovering = true;
        hovering = node;
      } else {
        node.hovering = false;
      }
    }

    if (hovering) {
      p5.cursor(p5.HAND);
      if (hovering !== elementInDetail) showDetailPanel(p5, hovering);
    } else if (selectedElement) {
      showDetailPanel(p5, selectedElement);
    } else if (!hovering) {
      p5.cursor(p5.ARROW);
      hideDetailPanel();
    }
  };

  // Zoom on mouse wheel
  p5.mouseWheel = function (event) {
    zoomFactor /= 1 + event.delta * 0.001;
    zoomFactor = p5.constrain(zoomFactor, 0.1, 5);

    return false; // Prevent default
  }

  // #endregion

  var zoomInButton = document.getElementById("btnZoomIn");
  zoomInButton.addEventListener("click", function () {
    zoomFactor *= 1.4;
    zoomFactor = p5.constrain(zoomFactor, 0.1, 5);
  });

  var zoomOutButton = document.getElementById("btnZoomOut");
  zoomOutButton.addEventListener("click", function () {
    zoomFactor /= 1.4;
    zoomFactor = p5.constrain(zoomFactor, 0.1, 5);
  });

  var homeButton = document.getElementById("btnHome");
  homeButton.addEventListener("click", function () {
    panning = p5.createVector(0, 0);
    zoomFactor = 1;
  });
});

const p5Matrix = new p5((p5) => {
  var canvasMatrix = null;
  p5.setup = function () {
    canvasMatrix = p5.createCanvas(368, 368);
    canvasMatrix.parent("matrixCanvasContainer");
    p5.background(255);

    p5.initMatrixVisualization(locations, nodes);
  };

  p5.initMatrixVisualization = function (locations, nodes) {
    matrixVisualization = new MatrixVisualization(locations, nodes, 368);
  };

  p5.draw = function () {
    p5.background(255);
    matrixVisualization.draw(p5, locations);
  };

  p5.doubleClicked = function () {
    matrixVisualization.doubleClicked(p5);
  };
});

setupFileInput(p5Graph, p5Matrix);
setupSettingsDropdown();
setupAgentPanel(p5Graph);

function applyAgentForces(p5, agents, currentSnapshot = 0) {
  for (let i = 0; i < agents.length; i++) {
    let agent1 = agents[i];
    let parent1 = agent1.snapshotParents[currentSnapshot].parentLocation
      .isUnfolded
      ? agent1.snapshotParents[currentSnapshot]
      : agent1.snapshotParents[currentSnapshot].parentLocation;

    applyGravityForce(p5, agent1, parent1.position, 5, false, false);
    if (agents.length === 1) return;

    for (let j = i + 1; j < agents.length; j++) {
      let agent2 = agents[j];
      let parent2 = agent2.snapshotParents[currentSnapshot].parentLocation
        .isUnfolded
        ? agent2.snapshotParents[currentSnapshot]
        : agent2.snapshotParents[currentSnapshot].parentLocation;
      if (parent1 === parent2) {
        applyRepulsionForce(p5, agent1, agent2, 60, false, false);
      }
    }
  }
}

function applyForces(p5, locations) {
  // Iterate over all pairs of locations
  for (let i = 0; i < locations.length; i++) {
    let location1 = locations[i];

    for (let j = i + 1; j < locations.length; j++) {
      let location2 = locations[j];

      // Apply repulsion force between all locations
      // Repulsion force is higher when the location is unfolded, by using location radii
      applyRepulsionForce(
        p5,
        location1,
        location2,
        repulsionFactor * (location1.radius + location2.radius),
        debug
      );

      applyAttractionForce(
        p5,
        location1,
        location2,
        weightFactor,
        debug,
        edgeThreshold,
        showStationaryState,
        normalizeStationaryDistribution ? maxSdEdgeWeight : 1
      );
    }

    // Apply repulsion force between all nodes in the location
    if (location1.isUnfolded) {
      let neighborNodes = location1.nodes;
      for (let k = 0; k < neighborNodes.length; k++) {
        let node1 = neighborNodes[k];
        for (let l = k + 1; l < neighborNodes.length; l++) {
          let node2 = neighborNodes[l];
          applyRepulsionForce(
            p5,
            node1,
            node2,
            repulsionIntralocationFactor / neighborNodes.length,
            debug
          );
        }

        // Apply axial force to nodes in unfolded locations
        node1.neighborLocations.forEach((_, neighborLocation) => {
          if (neighborLocation.isUnfolded) {
            neighborLocation.nodes.forEach((nodeInOtherLocation) => {
              applyAxialForce(
                p5,
                node1,
                nodeInOtherLocation,
                axialForceFactor,
                debug
              );
            });
          } else {
            applyAxialForce(
              p5,
              node1,
              neighborLocation,
              axialForceFactor,
              debug
            );
          }
        });

        applyGravityForce(
          p5,
          node1,
          location1.position,
          gravityIntralocationFactor,
          debug
        );
      }
    }

    applyGravityForce(
      p5,
      location1,
      p5.createVector(0, 0),
      gravityFactor,
      debug,
      true
    );

    if (debug) {
      drawForce(p5, location1, location1.force, "#000000");
    }
  }
}

function showDetailPanel(p5, element, regenerateSameElement = false) {
  if (!element || (!regenerateSameElement && element === elementInDetail))
    return;

  var detailPanel = document.getElementById("detailPanel");
  if (!detailPanel) return;

  elementInDetail = element;

  var detailPanelType = document.getElementById("detailPanelType");
  detailPanelType.innerHTML = `
    <div style="background-color: ${element.color};" class="bullet"></div>
    ${element.constructor.name} ${element.displayName === "" ? element.name : `<b>${element.displayName}</b> (${element.name})`}
  `;

  var detailPanelSdWeight = document.getElementById("detailPanelSdWeight");
  detailPanelSdWeight.innerHTML = `Time spent here: ${(
    element.stationaryDistributionWeight * 100
  ).toFixed(2)}%`;

  var detailPanelSdWeightBar = document.getElementById(
    "detailPanelSdWeightBar"
  );
  let color = p5.lerpColor(
    p5.color("#fff"),
    p5.color("#054A91"),
    element.stationaryDistributionWeight / maxSdLocationWeight
  );
  detailPanelSdWeightBar.innerHTML = `
    <div style="
      width:${element.stationaryDistributionWeight * 400}px; 
      height:10px;
      background: ${color};
      float: left;">
      </div>
    <div style="width:${400 - element.stationaryDistributionWeight * 400}px;
    height:10px;
    background-color: #f0f0f0;
    float: left;"></div>`;

  var detailPanelParentChildren = document.getElementById(
    "detailPanelParentChildren"
  );
  if (element.constructor.name === "Node") {
    detailPanelParentChildren.innerHTML = `
    <div>Parent location:</div>
    <div>${element.parentLocation.name}</div>`;
  } else {
    detailPanelParentChildren.innerHTML = `
      <div>Children nodes:</div>
      <div>${element.nodes.map((node) => `${node.name}`).join("<br />")}
      </div>`;
  }

  var detailPanelChartLabel = document.getElementById("detailPanelChartLabel");
  if (selectedElement) {
    detailPanelChartLabel.innerHTML = `Distribution of Recurring Visits<br />
    from: <div class="bullet" style="background-color: ${selectedElement.color};"></div> ${selectedElement.constructor.name} ${selectedElement.name}<br />
    to: <div class="bullet" style="background-color: ${element.color};"></div> ${element.constructor.name} ${element.name}`;
  } else {
    detailPanelChartLabel.innerHTML = `Distribution of Recurring Visits<br />
    from: <div class="bullet" style="background-color: ${element.color};"></div> ${element.constructor.name} ${element.name}<br />
    to: <div class="bullet" style="background-color: ${element.color};"></div> ${element.constructor.name} ${element.name}`;
  }

  var detailPanelChartContainer = document.getElementById(
    "detailPanelChartContainer"
  );
  let values = Array(snapshotsCount).fill(0);
  if (!selectedElement) {
    stateOverTime = generateStateOverTime(nodes, element, snapshotsCount);
  }
  if (element.constructor.name === "Location") {
    // Filter out the nodes from stateOverTime that are in the 'element' location
    // Then sum the values for each time step
    let locationIndices = element.nodes.map((node) => nodes.indexOf(node));
    stateOverTime.forEach((nodeStates, i) => {
      if (locationIndices.includes(i)) {
        nodeStates.forEach((state, j) => {
          values[j] += state;
        });
      }
    });
  } else if (element.constructor.name === "Node") {
    values = stateOverTime[nodes.indexOf(element)];
  }

  // Generate detailPanelChartContainer svg chart
  // The chart is a bar chart with the x-axis representing time and the y-axis representing the probability of being in the location or node
  const xAxisMajorMarkers = Array.from({ length: 11 }, (_, i) => i * 39.5 + 27);
  const xAxisMinorMarkers = Array.from(
    { length: 101 },
    (_, i) => i * 3.95 + 27
  );
  const yAxisMarkers = [0, 18.75, 37.5, 56.25, 74];

  const xAxisMajorMarkersSvg = xAxisMajorMarkers
    .map(
      (marker) =>
        `<line x1="${marker}" y1="75" x2="${marker}" y2="80" style="stroke:#000;stroke-width:1"/>`
    )
    .join("");

  const xAxisMajorMarkersTextSvg = [0, 10, 20, 30, , , 60, 70, 80, 90]
    .map((text, i) => {
      return `<text x="${xAxisMajorMarkers[i]}" y="87" text-anchor="middle" style="font-size: 7px;">${text}</text>`;
    })
    .join("");

  const xAxisMinorMarkersSvg = xAxisMinorMarkers
    .map(
      (marker) =>
        `<line x1="${marker}" y1="75" x2="${marker}" y2="77.5" style="stroke:#777;stroke-width:1"/>`
    )
    .join("");

  const yAxisMarkersSvg = yAxisMarkers
    .map(
      (marker) =>
        `<line x1="20" y1="${75 - marker}" x2="25" y2="${
          75 - marker
        }" style="stroke:#000;stroke-width:1" />`
    )
    .join("");

  const yAxisLinesSvg = yAxisMarkers
    .map(
      (marker) =>
        `<line x1="20" y1="${75 - marker}" x2="420" y2="${
          75 - marker
        }" style="stroke:#f0f0f0;stroke-width:1" />`
    )
    .join("");

  const xAxisSvg = `<line x1="20" y1="75" x2="420" y2="75" style="stroke:#000;stroke-width:1" />`;

  const yAxisSvg = `<line x1="25" y1="0" x2="25" y2="75" style="stroke:#000;stroke-width:1" />`;

  const currentSnapshotSvg = `<rect x="${
    currentSnapshot * 3.95 + 25
  }" y="0" width="4" height="75" fill="#f0f0f0" />`;

  detailPanelChartContainer.innerHTML = `
      ${
        currentSnapshotSvg +
        yAxisLinesSvg +
        xAxisMajorMarkersSvg +
        yAxisMarkersSvg +
        xAxisMinorMarkersSvg +
        xAxisMajorMarkersTextSvg +
        yAxisSvg
      }
        ${values
          .map((value, i) => {
            let color = p5.lerpColor(
              p5.color("#00F5DC"),
              p5.color("#009688"),
              value
            );
            return `<rect x="${i * 3.95 + 25}" y="${
              75 - value * 75
            }" width="4" height="${value * 75}" fill="${color}"></rect>`;
          })
          .join("")}
      ${xAxisSvg}
      <text x="210" y="90" text-anchor="middle">Steps</text>
      <text x="19" y="75" text-anchor="end" style="font-size: 7px;">0 %</text>
      <text x="19" y="7" text-anchor="end"  style="font-size: 7px;">100 %</text>
      `;

  detailPanel.style.display = "block";
}

function hideDetailPanel() {
  elementInDetail = null;
  document.getElementById("detailPanel").style.display = "none";
}

function resetAppState() {
  selectedElement = null;
  agents = [];
  currentSnapshot = 0;
  document.getElementById("agentPanel").style.display = "none";
}

function setupFileInput(p5Graph, p5Matrix) {
  // https://developer.mozilla.org/en-US/docs/Web/API/File_API/Using_files_from_web_applications

  // document.addEventListener("DOMContentLoaded", function () {
  const el = document.querySelectorAll("#fileWindow");
  const fileModal = M.Modal.init(el, {})[0];

  const fileInputButton = document.getElementById("fileInputButton");
  const fileInput = document.getElementById("fileInput");

  fileInputButton.addEventListener(
    "click",
    (_) => {
      if (fileInput) {
        fileInput.click();
      }
    },
    false
  );

  var reader = new FileReader();
  reader.onload = function (e) {
    var jsonFile = JSON.parse(e.target.result);
    fileModal.close();
    ({ nodes, locations, maxSdEdgeWeight, maxSdLocationWeight, background, name } =
      loadJsonExperiment(p5Graph, jsonFile));
    p5Matrix.initMatrixVisualization(locations, nodes);
    resetAppState();

    const filename = document
      .getElementById("fileInput")
      .value.split("\\")
      .pop()
      .split(".")
      .slice(0, -1)
      .join(".");

    document.getElementById("filename").innerHTML = name || filename;
  };

  fileInput.addEventListener("change", function () {
    var file = this.files[0];
    reader.readAsText(file);
  });

  document.getElementById("loadAirport").addEventListener("click", function () {
    fileModal.close();
    ({ nodes, locations, maxSdEdgeWeight, maxSdLocationWeight, background } =
      loadJsonExperiment(p5Graph, airportStrategy));
    p5Matrix.initMatrixVisualization(locations, nodes);
    resetAppState();
    document.getElementById("filename").innerHTML = "airport";
  });

  document
    .getElementById("loadOfficeNoMemory")
    .addEventListener("click", function () {
      fileModal.close();
      ({ nodes, locations, maxSdEdgeWeight, maxSdLocationWeight, background } =
        loadJsonExperiment(p5Graph, officeNoMemoryStrategy));
      p5Matrix.initMatrixVisualization(locations, nodes);
      resetAppState();
      document.getElementById("filename").innerHTML = "office_no_memory";
    });

  document.getElementById("loadSweep").addEventListener("click", function () {
    fileModal.close();
    ({ nodes, locations, maxSdEdgeWeight, maxSdLocationWeight, background } =
      loadJsonExperiment(p5Graph, sweepStrategy));
    p5Matrix.initMatrixVisualization(locations, nodes);
    resetAppState();
    document.getElementById("filename").innerHTML = "sweep";
  });
  // });
}

function setupAgentPanel(p5Graph) {
  // document.addEventListener("DOMContentLoaded", function () {
  var currentSnapshotSlider = document.getElementById("currentSnapshot");
  currentSnapshotSlider.value = currentSnapshot;
  currentSnapshotSlider.min = 0;
  currentSnapshotSlider.max = snapshotsCount - 1;
  currentSnapshotSlider.step = 1;
  currentSnapshotSlider.addEventListener("input", function () {
    currentSnapshot = parseInt(currentSnapshotSlider.value);
    showDetailPanel(p5Graph, selectedElement, true);
  });

  var showAgentsCheckbox = document.getElementById("showAgents");
  showAgentsCheckbox.addEventListener("change", function () {
    showAgents = !showAgents;

    var manyAgentsCheckbox = document.getElementById("manyAgentsCheckbox");
    var currentSnapshot = document.getElementById("currentSnapshot");

    manyAgentsCheckbox.disabled = !showAgents;
    currentSnapshot.disabled = !showAgents;

  });

  var manyAgentsCheckbox = document.getElementById("manyAgentsCheckbox");
  manyAgentsCheckbox.addEventListener("change", function () {
    if (manyAgentsCheckbox.checked) {
      manyAgents = true;
      agentsCount = 400;
      agents = [];
      if (selectedElement.constructor.name === "Location") {
        for (const node of selectedElement.nodes) {
          agents = agents.concat(
            initAgents(
              p5Graph,
              Math.floor(agentsCount / selectedElement.nodes.length),
              node,
              snapshotsCount
            )
          );
        }
      } else {
        agents = initAgents(
          p5Graph,
          agentsCount,
          selectedElement,
          snapshotsCount
        );
      }
    } else {
      manyAgents = false;
      agents = [];
      if (selectedElement.constructor.name === "Location") {
        agentsCount = selectedElement.nodes.length;
        for (const node of selectedElement.nodes) {
          agents = agents.concat(
            initAgents(
              p5Graph,
              Math.floor(agentsCount / selectedElement.nodes.length),
              node,
              snapshotsCount
            )
          );
        }
      } else {
        agentsCount = 1;
        agents = initAgents(
          p5Graph,
          agentsCount,
          selectedElement,
          snapshotsCount
        );
      }
      agents.forEach((agent) => {
        agent.radius = window.radiusObject.agentOneRadius;
      });
    }
  });
  // });
}

function setupEdgeThresholdSlider() {
  var edgeThresholdSlider = document.getElementById("edgeThreshold");

  if (normalizeStationaryDistribution && showStationaryState) {
    edgeThresholdSlider.max = 1;
    edgeThresholdSlider.step = 0.01;
    edgeThresholdSlider.value = edgeThreshold;
  } else {
    edgeThresholdSlider.max = 100;
    edgeThresholdSlider.step = 1;
    edgeThresholdSlider.value = edgeThreshold * 100;
  }
  edgeThresholdSlider.min = 0;

  function edgeThresholdSliderInputHandler() {
    if (normalizeStationaryDistribution && showStationaryState) {
      edgeThreshold = parseFloat(edgeThresholdSlider.value);
    } else {
      edgeThreshold = parseInt(edgeThresholdSlider.value) / 100;
    }

    if (showBSCC) {
      calculateBSCCs(locations, edgeThreshold);
    } else {
      locations.forEach((location) => {
        location.isBSCC = true;
      });
      nodes.forEach((node) => {
        node.isBSCC = true;
      });
    }

    // Shrink or expand the nodes and locations based on their accessibility.
    locations.forEach((location) => {
      if (location.isBSCC) {
        if (location.isUnfolded) {
          location.radius =
            window.radiusObject.locationOpenRadius *
            Math.sqrt(location.nodes.length);
        } else {
          location.radius = window.radiusObject.locationBaseRadius;
        }
      } else {
        if (location.isUnfolded) {
          location.radius =
            window.radiusObject.locationSmallOpenRadius *
            Math.sqrt(location.nodes.length);
        } else {
          location.radius = window.radiusObject.locationSmallBaseRadius;
        }
      }
    });
    nodes.forEach((node) => {
      if (node.isBSCC) {
        if (node.parentLocation.isUnfolded) {
          node.radius = window.radiusObject.nodeOpenRadius;
        } else {
          node.radius = window.radiusObject.nodeBaseRadius;
        }
      } else {
        if (node.parentLocation.isUnfolded) {
          node.radius = window.radiusObject.nodeSmallOpenRadius;
        } else {
          node.radius = window.radiusObject.nodeSmallBaseRadius;
        }
      }
    });
  }

  edgeThresholdSlider.removeEventListener(
    "input",
    edgeThresholdSliderInputHandler
  );
  edgeThresholdSlider.addEventListener(
    "input",
    edgeThresholdSliderInputHandler
  );
}

// https://materializecss.com/dropdown.html
function setupSettingsDropdown() {
  // document.addEventListener("DOMContentLoaded", function () {
  var elems = document.querySelectorAll(".dropdown-trigger");
  M.Dropdown.init(elems, {
    coverTrigger: false,
    alignment: "right",
    constrainWidth: false,
    closeOnClick: false,
  });

  elems = document.querySelectorAll(".tooltipped");
  M.Tooltip.init(elems, { position: "left" });

  elems = document.querySelectorAll(".tabs");
  M.Tabs.init(elems, {});

  // Connect tabs in the navbar to the showStationaryState variable
  var transitionsTab = document.getElementById("transitionsTab");
  transitionsTab.addEventListener("click", function () {
    showStationaryState = false;
    setupEdgeThresholdSlider();
  });
  var stationaryTab = document.getElementById("stationaryTab");
  stationaryTab.addEventListener("click", function () {
    showStationaryState = true;
    setupEdgeThresholdSlider();
  });

  // var edgeThresholdSlider = document.getElementById("edgeThreshold");
  // edgeThresholdSlider.value = edgeThreshold * 100;
  // edgeThresholdSlider.min = 0;
  // edgeThresholdSlider.max = 100;
  // edgeThresholdSlider.step = 1;
  // edgeThresholdSlider.addEventListener("input", function () {
  //   if (normalizeStationaryDistribution && showStationaryState) {
  //     edgeThreshold = parseInt(edgeThresholdSlider.value);
  //   } else {
  //     edgeThreshold = parseInt(edgeThresholdSlider.value) / 100;
  //   }

  //   if (showBSCC) {
  //     calculateBSCCs(locations, edgeThreshold);
  //   } else {
  //     locations.forEach((location) => {
  //       location.isBSCC = true;
  //     });
  //     nodes.forEach((node) => {
  //       node.isBSCC = true;
  //     });
  //   }

  //   // Shrink or expand the nodes and locations based on their accessibility.
  //   locations.forEach((location) => {
  //     if (location.isBSCC) {
  //       if (location.isUnfolded) {
  //         location.radius =
  //           window.radiusObject.locationOpenRadius *
  //           Math.sqrt(location.nodes.length);
  //       } else {
  //         location.radius = window.radiusObject.locationBaseRadius;
  //       }
  //     } else {
  //       if (location.isUnfolded) {
  //         location.radius =
  //           window.radiusObject.locationSmallOpenRadius *
  //           Math.sqrt(location.nodes.length);
  //       } else {
  //         location.radius = window.radiusObject.locationSmallBaseRadius;
  //       }
  //     }
  //   });
  //   nodes.forEach((node) => {
  //     if (node.isBSCC) {
  //       if (node.parentLocation.isUnfolded) {
  //         node.radius = window.radiusObject.nodeOpenRadius;
  //       } else {
  //         node.radius = window.radiusObject.nodeBaseRadius;
  //       }
  //     } else {
  //       if (node.parentLocation.isUnfolded) {
  //         node.radius = window.radiusObject.nodeSmallOpenRadius;
  //       } else {
  //         node.radius = window.radiusObject.nodeSmallBaseRadius;
  //       }
  //     }
  //   });
  // });

  setupEdgeThresholdSlider();

  var foldLocationsButton = document.getElementById("foldLocationsButton");
  foldLocationsButton.addEventListener("click", function () {
    locations.forEach((location) => {
      location.setUnfold(false);
    });
  });

  var unfoldLocationsButton = document.getElementById("unfoldLocationsButton");
  unfoldLocationsButton.addEventListener("click", function () {
    locations.forEach((location) => {
      location.setUnfold(true);
    });
  });

  // Connect enable forces checkbox defined in the html file to the enableForces variable
  var forcesCheckbox = document.getElementById("forcesCheckbox");
  forcesCheckbox.addEventListener("change", function () {
    enableForces = !enableForces;
  });

  // Connect show BSCC checkbox defined in the html file to the showBSCC variable
  var bsccCheckbox = document.getElementById("bsccCheckbox");
  bsccCheckbox.addEventListener("change", function () {
    showBSCC = !showBSCC;

    if (showBSCC) {
      calculateBSCCs(locations, edgeThreshold);
    } else {
      locations.forEach((location) => {
        location.isBSCC = true;
      });
      nodes.forEach((node) => {
        node.isBSCC = true;
      });
    }

    // Shrink or expand the nodes and locations based on their accessibility.
    locations.forEach((location) => {
      if (location.isBSCC) {
        if (location.isUnfolded) {
          location.radius =
            window.radiusObject.locationOpenRadius *
            Math.sqrt(location.nodes.length);
        } else {
          location.radius = window.radiusObject.locationBaseRadius;
        }
      } else {
        if (location.isUnfolded) {
          location.radius =
            window.radiusObject.locationSmallOpenRadius *
            Math.sqrt(location.nodes.length);
        } else {
          location.radius = window.radiusObject.locationSmallBaseRadius;
        }
      }
    });
    nodes.forEach((node) => {
      if (node.isBSCC) {
        if (node.parentLocation.isUnfolded) {
          node.radius = window.radiusObject.nodeOpenRadius;
        } else {
          node.radius = window.radiusObject.nodeBaseRadius;
        }
      } else {
        if (node.parentLocation.isUnfolded) {
          node.radius = window.radiusObject.nodeSmallOpenRadius;
        } else {
          node.radius = window.radiusObject.nodeSmallBaseRadius;
        }
      }
    });
  });

  // Connect checkbox that controls whether to normalize stationary distribution weights
  var normalizeStationaryDistributionCheckbox = document.getElementById(
    "normalizeStationaryDistributionCheckbox"
  );
  normalizeStationaryDistributionCheckbox.addEventListener(
    "change",
    function () {
      normalizeStationaryDistribution = !normalizeStationaryDistribution;

      setupEdgeThresholdSlider();
    }
  );

  // Connect show connection weights checkbox defined in the html file to the showEdgeNumbers variable
  var connectionWeightsCheckbox = document.getElementById("showWeights");
  connectionWeightsCheckbox.addEventListener("change", function () {
    showEdgeNumbers = !showEdgeNumbers;
  });

  // Connect gravity slider defined in the html file to the gravityFactor variable
  var gravitySlider = document.getElementById("gravitySlider");
  gravitySlider.min = 0;
  gravitySlider.max = 0.5;
  gravitySlider.step = 0.01;
  gravitySlider.value = gravityFactor;
  gravitySlider.addEventListener("input", function () {
    gravityFactor = parseFloat(gravitySlider.value);
  });

  // Connect repulsion slider defined in the html file to the repulsionFactor variable
  var repulsionSlider = document.getElementById("repulsionSlider");
  repulsionSlider.min = 1;
  repulsionSlider.max = 7;
  repulsionSlider.step = 0.25;
  repulsionSlider.value = Math.log10(repulsionFactor);
  repulsionSlider.addEventListener("input", function () {
    repulsionFactor = 10 ** parseFloat(repulsionSlider.value);
  });

  // Connect gravity intralocation slider defined in the html file to the gravityIntralocationFactor variable
  var gravityIntralocationSlider = document.getElementById(
    "gravityIntralocationSlider"
  );
  gravityIntralocationSlider.min = 0;
  gravityIntralocationSlider.max = 10;
  gravityIntralocationSlider.step = 0.1;
  gravityIntralocationSlider.value = gravityIntralocationFactor;
  gravityIntralocationSlider.addEventListener("input", function () {
    gravityIntralocationFactor = parseFloat(gravityIntralocationSlider.value);
  });

  // Connect repulsion intralocation slider defined in the html file to the repulsionIntralocationFactor variable
  var repulsionIntralocationSlider = document.getElementById(
    "repulsionIntralocationSlider"
  );
  repulsionIntralocationSlider.min = 1;
  repulsionIntralocationSlider.max = 7;
  repulsionIntralocationSlider.step = 0.25;
  repulsionIntralocationSlider.value = Math.log10(repulsionIntralocationFactor);
  repulsionIntralocationSlider.addEventListener("input", function () {
    repulsionIntralocationFactor =
      10 ** parseFloat(repulsionIntralocationSlider.value);
  });

  // Connect axial force slider defined in the html file to the axialForceFactor variable
  var axialSlider = document.getElementById("axialSlider");
  axialSlider.min = -2;
  axialSlider.max = 3;
  axialSlider.step = 0.25;
  axialSlider.value = Math.log10(axialForceFactor);
  axialSlider.addEventListener("input", function () {
    axialForceFactor = 10 ** parseFloat(axialSlider.value);
  });

  // Connect edge weight importance slider defined in the html file to the weightFactor variable
  var weightSlider = document.getElementById("weightSlider");
  weightSlider.min = 0;
  weightSlider.max = 2;
  weightSlider.step = 0.1;
  weightSlider.value = weightFactor;
  weightSlider.addEventListener("input", function () {
    weightFactor = parseFloat(weightSlider.value);
  });

  // Connect simulation step slider defined in the html file to the simulationStep variable
  var simulationStepSlider = document.getElementById("simulationStepSlider");
  simulationStepSlider.min = 0;
  simulationStepSlider.max = 0.5;
  simulationStepSlider.step = 0.05;
  simulationStepSlider.value = simulationStep;
  simulationStepSlider.addEventListener("input", function () {
    simulationStep = parseFloat(simulationStepSlider.value);
  });

  // Connect restore button to restore sliders to default values
  var restoreButton = document.getElementById("restoreButton");
  restoreButton.addEventListener("click", function () {
    gravityFactor = 0.1;
    gravitySlider.value = gravityFactor;
    repulsionFactor = 500;
    repulsionSlider.value = Math.log10(repulsionFactor);
    gravityIntralocationFactor = 1;
    gravityIntralocationSlider.value = gravityIntralocationFactor;
    repulsionIntralocationFactor = 10000;
    repulsionIntralocationSlider.value = Math.log10(
      repulsionIntralocationFactor
    );
    weightFactor = 1;
    weightSlider.value = weightFactor;
    axialForceFactor = 0.1;
    axialSlider.value = Math.log10(axialForceFactor);
    simulationStep = 0.05;
    simulationStepSlider.value = simulationStep;
  });

  // Connect debug checkbox defined in the html file to the debug variable
  var debugCheckbox = document.getElementById("debugCheckbox");
  debugCheckbox.addEventListener("change", function () {
    debug = !debug;
  });
  // });
}

const panelCollapsed = {};

setInterval(() => {
  [...document.getElementsByClassName("card")].forEach((card) => {
    const cardHeader = card.getElementsByClassName("card-title")[0];
    const cardContent = card.getElementsByClassName("card-content")[0];

    if (!cardHeader || !cardContent) return;

    const update = (id) => {
      const value = panelCollapsed[id];

      let caret = cardHeader.getElementsByClassName("material-icons")[0];

      if (!caret) {
        // Create caret icon
        caret = document.createElement("i");
        caret.className = "material-icons right";
        caret.textContent = "expand_more";
        cardHeader.appendChild(caret);
      }

      if (value) {
        cardContent.classList.remove("collapsed");
        caret.textContent = "expand_less";
      } else {
        cardContent.classList.add("collapsed");
        caret.textContent = "expand_more";
      }
    };

    const toggle = (id) => {
      const from = panelCollapsed[id] === undefined ? true : panelCollapsed[id];
      
      panelCollapsed[id] = !from;
      update(id);
    }

    if (!card.id) {
      card.id = `panel-${Math.random().toString(36).substring(7)}`;
      panelCollapsed[card.id] = true;
      update(card.id);
      cardHeader.addEventListener("click", () => toggle(card.id));
    }

    if (!cardHeader.getElementsByClassName("material-icons")[0]) {
      update(card.id);
    }
  });

}, 100);
