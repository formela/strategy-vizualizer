# Strategy Visualizer

This application visualizes patrolling strategies.

[**RUN THE DEMO**](https://formela.pages.fi.muni.cz/strategy-vizualizer/)

## Run Locally
* Install Node.js.
* Clone the repository.
* Open the root folder in terminal.
```
> npm install
> npm run start
```
* Open [http://localhost:8080](http://localhost:8080) in a browser.

## Build Locally
* Follow steps above to run the app.
```
> npm run build
```
* The app is in `dist` folder.
* Open `dist/index.html` in a browser.

## Use Strategy Database

The application can access the RegSTAR database directly to load and visualize all stored strategies.
To access the database, first run the backend responsible for the database connection availble [here](https://gitlab.fi.muni.cz/formela/regstar-visualization).
Run the backend using the instructions in Regstar Visualization.
Once the backend is running, you can load any strategy using query strings:

* `id`: ID of the run.
* `url` (optional): URL of the backend API access point. If not provided, `http://localhost:5000` will be used.

Example web address: [https://formela.pages.fi.muni.cz/strategy-vizualizer/?id=6671f645b193487306fca8f5](https://formela.pages.fi.muni.cz/strategy-vizualizer/?id=6671f645b193487306fca8f5).

It is best to use the connection to database together with RegSTAR Dashboard that creates the links automatically.